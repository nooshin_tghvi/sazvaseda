<?php

return [
    'token' => env('FARAZ_SMS_TOKEN'),
    'originator' => env('FARAZ_SMS_ORIGINATOR')
];
