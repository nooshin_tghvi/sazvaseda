<?php


return [
    "first_name" => 75,
    "last_name" => 75,
    "full_name" => 150,
    "name" => 120,
    "email" => 150,
    "mobile" => 11,
    "melli_code" => 10,
    "iban" => 26,           //International Bank Account number
    "text" => 21500,        //char
    "file_path" => 191,     //default
    "address" => 191,       //default
    "card_number" => 16,
    "factor_number" => 10,  //for transaction
    "amount" => 15          //OR price, $table->decimal('amount', $precision = 15, $scale = 0);
];
