<?php

use App\Http\Controllers\ArtistController;
use App\Http\Controllers\ArtistPortfolioController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\SingingTestController;
use App\Http\Controllers\StudioController;
use App\Http\Controllers\TitleController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('Hello_World', function () {
    return response('API) Hello World');
});

Route::get('PostManFile', function () {
    return url('file/SazVaSeda.postman_collection.json');
});
Route::get('DBFile', function () {
    return url('file/sazvaseda.sql');
});

Route::get('/unauthorized', function () {
    return response(['error' => true, 'messages' => ['Plz Login first']], 401);
})->name('login.Plz');

Route::prefix('search/')->group(function () {
    Route::get('artist', [ArtistController::class, 'search']);
});

Route::get('artists', [ArtistController::class, 'get']);
Route::get('artist/{artistId}/detail', [ArtistController::class, 'detail'])->name('artist.detail');

Route::get('packages', [PackageController::class, 'get']);
Route::get('package/{packageId}/detail', [PackageController::class, 'detail'])->name('package.detail');

Route::get('studios', [StudioController::class, 'get']);
Route::get('studio/{studioId}/detail', [StudioController::class, 'detail'])->name('studio.detail');

Route::get('titles', [TitleController::class, 'get']);
Route::get('title/{titleId}/detail', [TitleController::class, 'detail'])->name('title.detail');

Route::get('provinces', [CityController::class, 'getProvinces']);
Route::get('counties', [CityController::class, 'getCounties']);
Route::get('cities', [CityController::class, 'getCities']);
Route::get('city/{cityId}/detail', [CityController::class, 'detail'])->name('title.detail');

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:user');
Route::post('forgot/password', [UserController::class, 'forgotPassword']);

Route::middleware('auth:user')->group(function () {
    Route::get('me', [AuthController::class, 'me']);
    Route::post('first_name', [AuthController::class, 'changeFirstName']);
    Route::post('last_name', [AuthController::class, 'changeLastName']);
    Route::post('email', [AuthController::class, 'changeEmail']);
    Route::post('password', [AuthController::class, 'changePassword']);
    Route::post('mobile', [UserController::class, 'changeMobile']);
    Route::patch('has_studio', [UserController::class, 'hasStudio']);

    Route::put('artist/register', [ArtistController::class, 'register']);
    Route::get('artist/me', [ArtistController::class, 'me']);
    Route::post('melli_code', [UserController::class, 'completeMelliCode']);
    Route::post('iban', [UserController::class, 'completeIBAN']);
    Route::post('avatar', [ArtistController::class, 'completeAvatar']);
    Route::post('experience', [ArtistController::class, 'completeExperience']);
    Route::post('order_description', [ArtistController::class, 'completeOrderDescription']);
    Route::post('delivery_time', [ArtistController::class, 'completeDeliveryTime']);
    Route::post('accept_advisor', [ArtistController::class, 'changeAcceptAdvisor']);
    Route::post('advise_price', [ArtistController::class, 'completeAdvisePrise']);
});

Route::prefix('title/')->middleware('auth:user')->group(function () {
    Route::get('my', [TitleController::class, 'myTitles']);
    Route::post('artist', [TitleController::class, 'addTitleForArtist']);
    Route::patch('artist/edit/{titleId}', [TitleController::class, 'editTitleForArtist']);
    Route::delete('artist/{titleId}', [TitleController::class, 'deleteTitleFromArtist']);
    Route::patch('{titleId}/description', [TitleController::class, 'editDescription']);
    Route::patch('{titleId}/accept_order', [TitleController::class, 'changeAcceptOrder']);
    Route::patch('{titleId}/order_price', [TitleController::class, 'editOrderPrise']);
});

Route::prefix('portfolio/artist/')->middleware('auth:user')->group(function () {
    Route::post('', [ArtistPortfolioController::class, 'register']);
    Route::delete('{id}', [ArtistPortfolioController::class, 'delete']);
    Route::get('{id}/detail', [ArtistPortfolioController::class, 'detail']);
    Route::post('edit/{id}', [ArtistPortfolioController::class, 'edit']);
    Route::patch('{id}/name', [ArtistPortfolioController::class, 'changeName']);
    Route::patch('{id}/date', [ArtistPortfolioController::class, 'changeDate']);
    Route::patch('{id}/saleability', [ArtistPortfolioController::class, 'changeSaleability']);
    Route::patch('{id}/description', [ArtistPortfolioController::class, 'changeDescription']);
    Route::post('{id}/file', [ArtistPortfolioController::class, 'changeFiles']);
});

Route::prefix('studio/')->middleware('auth:user')->group(function () {
    Route::get('my', [StudioController::class, 'myStudios']);
    Route::get('myDetail/{studioId}', [StudioController::class, 'detailsForOwner']);
    Route::post('register', [StudioController::class, 'register']);
    Route::delete('{studioId}', [StudioController::class, 'delete']);
    Route::patch('{studioId}/name', [StudioController::class, 'changeName']);
//    Route::patch('{studioId}/logo', [StudioController::class, 'changeLogo']);
    Route::patch('{studioId}/city', [StudioController::class, 'changeCity']);
    Route::patch('{studioId}/address', [StudioController::class, 'changeAddress']);
    Route::patch('{studioId}/price', [StudioController::class, 'changePrice']);
    Route::patch('{studioId}/description', [StudioController::class, 'changeDescription']);
    Route::patch('{studioId}/activation', [StudioController::class, 'changeActivation']);

    Route::post('{studioId}/image', [StudioController::class, 'addImage']);
    Route::delete('{studioId}/image/{imageId}', [StudioController::class, 'deleteImage']);
    Route::patch('{studioId}/image/{imageId}/name', [StudioController::class, 'changeImageName']);
    Route::patch('{studioId}/image/{imageId}/description', [StudioController::class, 'changeImageDescription']);
    Route::post('{studioId}/image/{imageId}/file', [StudioController::class, 'changeImageFile']);
});

Route::prefix('cart/')->middleware('auth:user')->group(function () {
    Route::get('', [CartController::class, 'get']);
    Route::put('{type}', [CartController::class, 'addItemToCart']);
    Route::delete('{type}', [CartController::class, 'deleteItemFromCart']);
    Route::get('order', [CartController::class, 'myTransaction']); //todo check
});

Route::prefix('discount/')->middleware('auth:user')->group(function () {
    Route::get('check', [DiscountController::class, 'isValidCode']);
});

Route::prefix('buy/')->middleware('auth:user')->group(function () {
    Route::get('', [TransactionController::class, 'buy']);
});

Route::prefix('pay/')->group(function () {
    Route::get('verify/{factorNumber}', [TransactionController::class, 'verify']);
});

Route::prefix('sms/')->group(function () {
    Route::post('send/{action}', [UserController::class, 'sendSMSFor']);
    Route::patch('verify/{action}', [UserController::class, 'verifyMobileFor']);
});

Route::post('contact_us', [ContactUsController::class, 'create']);

Route::prefix('singing/')->middleware('auth:user')->group(function () {
    Route::post('', [SingingTestController::class, 'register']);
    Route::get('', [SingingTestController::class, 'mySingingTest']);
});

Route::prefix('reservation/')->group(function () {
    Route::get('allowed/hrs', [ReservationController::class, 'getAllowedHours']);
    Route::prefix('advisor/')->group(function () {
        Route::get('search', [ReservationController::class, 'searchInAdviser']);
        Route::get('', [ReservationController::class, 'getReservationForAdvisor']);
        Route::get('past', [ReservationController::class, 'getPastReservationForAdvisor']);
        Route::put('', [ReservationController::class, 'addTimeForAdvisor'])->middleware('auth:user');
        Route::delete('', [ReservationController::class, 'removeTimeForAdvisor'])->middleware('auth:user');
    });
    Route::prefix('studio/')->group(function () {
        Route::get('search', [ReservationController::class, 'searchInStudio']);
        Route::get('', [ReservationController::class, 'getReservationForStudio']);
        Route::get('past', [ReservationController::class, 'getPastReservationForStudio']);
        Route::put('', [ReservationController::class, 'addTimeForStudio'])->middleware('auth:user');
        Route::delete('', [ReservationController::class, 'removeTimeForStudio'])->middleware('auth:user');
    });
});
Route::post('score/reservation', [ReservationController::class, 'recordScore'])->middleware('auth:user');

Route::prefix('status/reservation/')->middleware('auth:user')->group(function () {
    Route::get('user', [ReservationController::class, 'getStatusOfReservationForUser']);
});

//Route::prefix('search/')->group(function () {
//    Route::get('artist', function () {
//        $query = ''; // <-- Change the query for testing.
//
//        return \App\Models\Artist::search($query)->get();
//    });
//});
