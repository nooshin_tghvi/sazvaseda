<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class NewEvent extends Notification
{
    use Queueable;

    private string $content;
    private string $channel;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($channel, $content)
    {
        $this->channel = $channel;
        $this->content = $content;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable): SlackMessage
    {
        return (new SlackMessage)->to($this->channel)->content($this->content);
    }
}
