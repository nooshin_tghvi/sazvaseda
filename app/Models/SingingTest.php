<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SingingTest extends Model
{
    use HasFactory;

    protected $table = 'singing_tests';

    protected $fillable = ['user_id', 'file', 'description'];

    protected $hidden = ['has_been_answered', 'created_at', 'updated_at'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
