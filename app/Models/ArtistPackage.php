<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class ArtistPackage extends Model
{
    use HasFactory;

    protected $table = 'artist_package';

    protected $fillable = ['artist_id', 'package_id', 'title_id'];

    protected $hidden = ['accept'];

    public function package(): BelongsTo
    {
        return $this->belongsTo(Package::class);
    }

    public function artist(): BelongsTo
    {
        return $this->belongsTo(Artist::class);
    }
}
