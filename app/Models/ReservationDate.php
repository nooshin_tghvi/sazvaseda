<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;


/**
 * @method static find($res_date_id)
 */
class ReservationDate extends Model
{
    use HasFactory;

    protected $fillable = ['date', 'owner_type', 'owner_id'];

    protected $hidden = ['id', 'owner_type', 'owner_id', 'date', 'created_at', 'updated_at'];

    protected $dates = ['date'];
    protected $dateFormat = 'Y-m-d';

    public function owner(): MorphTo
    {
        return $this->morphTo();
    }

    public function reservationDetails(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ReservationDetail::class, 'res_date_id');
    }
}
