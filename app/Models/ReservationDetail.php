<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property mixed reservationDate
 * @property mixed allowedHour
 */
class ReservationDetail extends Model
{
    use HasFactory;

    protected $fillable = ['res_date_id', 'allowed_hr_id'];

    protected $hidden = ['res_date_id', 'allowed_hr_id', 'user_id', 'reservationDate', 'created_at', 'updated_at'];

    public function reservationDate(): BelongsTo
    {
        return $this->belongsTo(ReservationDate::class, 'res_date_id');
    }

    public function allowedHour(): BelongsTo
    {
        return $this->belongsTo(AllowedHour::class, 'allowed_hr_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
