<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static find($province_id)
 */
class Province extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    protected $visible = ['id', 'name'];

    protected $hidden = ['created_at', 'updated_at'];
}
