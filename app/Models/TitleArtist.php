<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, mixed $id)
 * @method static create(array $array)
 * @method static find($id)
 */
class TitleArtist extends Model
{
    use HasFactory;

    protected $table = 'title_artist';

    protected $fillable = ['artist_id', 'title_id', 'description', 'accept_order', 'order_price'];

    protected $hidden = ['artist_id', 'created_at', 'updated_at'];
}
