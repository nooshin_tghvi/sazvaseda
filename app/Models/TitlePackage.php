<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

class TitlePackage extends Model
{
    use HasFactory;

    protected $table = 'title_package';

    protected $fillable = ['artist_package_id', 'title_id'];

    public function title(): BelongsTo
    {
        return $this->belongsTo(Title::class);
    }

    public function artistPackage(): BelongsTo
    {
        return $this->belongsTo(ArtistPackage::class);
    }

    public function package(): hasOneThrough
    {
        return $this->hasOneThrough(Package::class, ArtistPackage::class,
            'id', 'id', 'artist_package_id', 'package_id');
    }

    public function artist(): hasOneThrough
    {
        return $this->hasOneThrough(Artist::class, ArtistPackage::class,
            'id', 'id', 'artist_package_id', 'artist_id');
    }
}
