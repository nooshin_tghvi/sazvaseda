<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @method static where(string $string, mixed $user_id)
 * @method static paginate(int|mixed $rrp)
 * @method static create(array $array)
 * @method static find($id)
 * @method static reject(\Closure $param)
 * @method static min(string $string)
 * @method static max(string $string)
 * @property mixed experience
 * @property bool|mixed accept_order
 * @property mixed order_description
 * @property mixed delivery_time
 * @property bool|mixed is_advisor
 * @property mixed advise_price
 * @property mixed avatar
 * @property mixed id
 * @property mixed titles
 * @property mixed user
 */
class Artist extends Model
{
    use HasFactory;

    protected $fillable = ['user_id',];

    protected $hidden = ['user_id', 'user', 'packages', 'contents', 'titles', 'status', 'created_at', 'updated_at'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function titles(): BelongsToMany
    {
        return $this->belongsToMany(Title::class, 'title_artist')
            ->withPivot('id', 'description', 'accept_order', 'order_price');
    }

    public function packages(): BelongsToMany
    {
        return $this->belongsToMany(Package::class, 'artist_package');
    }

    public function contents(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(FileBank::class, 'owner');
    }
}
