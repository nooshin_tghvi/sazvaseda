<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $fillable = ['first_name', 'last_name', 'mobile', 'email', 'password'];

    protected $hidden = ['created_at', 'updated_at', 'password', 'remember_token'];

    protected $casts = ['email_verified_at' => 'datetime'];


}
