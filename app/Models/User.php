<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * @method static where(string $string, mixed $input)
 * @method static create(array $toArray)
 * @property mixed id
 * @property mixed LastUnpaidCart
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed mobile
 * @property mixed singingTests
 * @property mixed studios
 * @property mixed artist
 * @property mixed reservationDetails
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = ['first_name', 'last_name', 'mobile', 'email', 'password'];

    protected $hidden = ['stock', 'is_approved', 'created_at', 'updated_at', 'password', 'remember_token',
        'email_verified_at', 'artist']; // 'email','melli_code', 'iban'

    protected $casts = ['email_verified_at' => 'datetime'];

    public function fullName(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function artist(): HasOne
    {
        return $this->hasOne(Artist::class);
    }

    public function packages(): BelongsToMany
    {
        return $this->belongsToMany(Package::class);
    }

    public function studios(): HasMany
    {
        return $this->hasMany(Studio::class);
    }

    public function LastUnpaidCart(): HasOne
    {
        return $this->hasOne(Cart::class)->where('is_pay', false);
    }

    public function carts(): HasMany
    {
        return $this->hasMany(Cart::class);
    }

    public function singingTests(): hasMany
    {
        return $this->hasMany(SingingTest::class);
    }

    public function reservationDetails(): HasMany
    {
        return $this->hasMany(ReservationDetail::class, 'user_id');
    }
}
