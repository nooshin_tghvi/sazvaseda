<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, $provinceId)
 */
class City extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'province_id', 'county_id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function province(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function county(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(County::class, 'county_id');
    }

    public function studios(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Studio::class);
    }
}
