<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 * @method static where(string $string, $id)
 */
class ItemCart extends Model
{
    use HasFactory;

    protected $table = 'item_cart';

    protected $fillable = ['cart_id', 'type', 'item_id'];
}
