<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 * @method static find($id)
 * @method static where(string $string, $id)
 */
class FileBank extends Model
{
    use HasFactory;

    protected $fillable = ['owner_type', 'owner_id', 'name', 'date', 'is_saleable', 'description', 'type', 'url'];

    protected $hidden = ['is_saleable', 'amount', 'owner_type', 'owner_id', 'created_at', 'updated_at'];

    protected $casts = [
        'date' => 'datetime:Y/m/d',
    ];

    public function owner(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }
}
