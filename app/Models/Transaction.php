<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static where(string $string, string $factorNumber)
 * @method static create(array $array)
 */
class Transaction extends Model
{
    use HasFactory;

    protected $fillable = ['amount', 'factor_number', 'cart_id', 'discount_id'];

    protected $hidden = [];

    public function cart(): BelongsTo
    {
        return $this->belongsTo(Cart::class);
    }

    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }
}
