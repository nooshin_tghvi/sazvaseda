<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * @method static where(string $string, bool $true)
 * @method static find($itemId)
 * @property mixed id
 */
class Package extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'user_id', 'price', 'delivery_time', 'image'];

    protected $hidden = ['user_id', 'artists', 'contents', 'is_viewable', 'is_active', 'created_at', 'updated_at'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function artists(): BelongsToMany
    {
        return $this->belongsToMany(Artist::class, 'artist_package')->withPivot('accept');
    }

    public function contents(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(FileBank::class, 'owner');
    }

    public function titles(): HasManyThrough
    {
        return $this->hasManyThrough(TitlePackage::class, ArtistPackage::class);
    }
}
