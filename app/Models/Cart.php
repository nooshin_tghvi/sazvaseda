<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

/**
 * @method static where(string $string, $userId)
 * @method static create(array $array)
 */
class Cart extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'details'];

    protected $hidden = ['id', 'user_id', 'is_pay', 'transaction_id', 'created_at', 'updated_at'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function items($carId): \Illuminate\Support\Collection
    {
        return DB::table('item_cart')->where('cart_id', $carId)->get();
    }
}
