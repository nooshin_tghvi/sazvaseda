<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    use HasFactory;

    protected $fillable = ['full_name', 'email', 'subject', 'description', 'tracking_no', 'opened_at'];

    protected $hidden = [];

    protected $casts = [
        'opened_at' => 'datetime',
    ];
}
