<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static firstOrCreate(string[] $array, array $array1)
 * @method static where(string $string, mixed $input)
 */
class SMSVerify extends Model
{
    use HasFactory;

    protected $table = 'sms_verify';

    protected $fillable = ['mobile'];

    protected $hidden = ['created_at', 'updated_at'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
