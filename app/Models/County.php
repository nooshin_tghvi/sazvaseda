<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, $provinceId)
 * @method static find($county_id)
 */
class County extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'province_id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function province(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
}
