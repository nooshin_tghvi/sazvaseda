<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * @method static where(string $string, $titleId)
 * @method static orderBy(mixed|string $column, mixed|string $direction)
 * @method static find($title_id)
 * @property mixed id
 */
class Title extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    protected $hidden = ['packages', 'artists', 'created_at', 'updated_at'];

    public function artists(): BelongsToMany
    {
        return $this->belongsToMany(Artist::class, 'title_artist')
            ->withPivot('description', 'accept_order', 'order_price');
    }

    public function packages(): HasManyThrough
    {
        return $this->hasManyThrough(Package::class, ArtistPackage::class);
    }
}
