<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, mixed $input)
 * @method static find($discount_id)
 */
class Discount extends Model
{
    use HasFactory;

    protected $fillable = ['code', 'type', 'value', 'maximum_value', 'expire_date', 'count',];

    protected $visible = ['code', 'type', 'value', 'maximum_value', 'expire_date', 'count', 'used_number',];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['expire_date'];

//    protected $dateFormat = 'Y-m-d';
//    protected $casts = ['expire_date' => 'date'];
}
