<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @method static create(array $array)
 * @method static where(string $string, $imageId)
 * @method static find($imageId)
 */
class Image extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'owner_type', 'owner_id', 'path'];

    protected $hidden = ['owner_type', 'owner_id', 'created_at', 'updated_at'];

    public function owner(): MorphTo
    {
        return $this->morphTo();
    }

}
