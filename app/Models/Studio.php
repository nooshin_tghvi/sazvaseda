<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static where(string $string, bool $true)
 * @method static create(array $array)
 * @method static find($id)
 * @method static max(string $string)
 * @method static min(string $string)
 * @property mixed city
 * @property mixed id
 */
class Studio extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'user_id', /*'logo',*/ 'city_id', 'address', 'price'];

    protected $hidden = ['user_id', 'city', 'city_id', 'images', 'status', 'created_at', 'updated_at'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function images(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Image::class, 'owner');
    }

    public function city(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(City::class);
    }
}
