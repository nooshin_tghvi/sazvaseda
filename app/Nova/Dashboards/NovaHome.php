<?php

namespace App\Nova\Dashboards;

use Laravel\Nova\Dashboard;

class NovaHome extends Dashboard
{
    /**
     * Get the cards for the dashboard.
     *
     * @return array
     */
    public function cards(): array
    {
        return [
            //
        ];
    }

    /**
     * Get the URI key for the dashboard.
     *
     * @return string
     */
    public static function uriKey(): string
    {
        return 'home';
    }

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label(): string
    {
        return __('Home');
    }
}
