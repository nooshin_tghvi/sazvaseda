<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

class Artist extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Artist::class;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'اشخاص';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['id',];

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label(): string
    {
        return __('Artist');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            BelongsTo::make(__('User'), 'user', 'App\Nova\User')
                ->sortable(),
            Avatar::make(__('Image'), 'avatar')
                ->disk('public')
                ->rules('max:2500'),
            Select::make(__('Status'), 'status')->options([
                'PENDING' => __('Pending'),
                'APPROVAL' => __('Approval'),
                'FAILED' => __('Failed'),
            ])->displayUsingLabels()
                ->sortable()
                ->rules('required', Rule::in(['PENDING', 'APPROVAL', 'FAILED'])),
            Textarea::make(__('Experience'), 'experience')
                ->nullable(),
            Textarea::make(__('Order Description'), 'order_description')
                ->nullable(),
            Number::make(__('Delivery Time'), 'delivery_time')
                ->sortable()
                ->rules('required', 'integer')
                ->default(0),
            Boolean::make(__('Is Advisor'), 'is_advisor')
                ->sortable(),
            Number::make(__('Advise Price'), 'advise_price')
                ->sortable()
                ->rules('integer', 'min:0')
                ->default(0),
            BelongsToMany::make(__('Titles'), 'titles', 'App\Nova\Title')
                ->fields(function () {
                    return [
                        Boolean::make(__('Accept Order'), 'accept_order')
                            ->sortable(),
                        Number::make(__('Order Price'), 'order_price')
                            ->sortable()
                            ->rules('integer', 'min:0')
                            ->default(0),
                        Text::make(__('Description'), 'description')
                            ->nullable()
                            ->rules('max:191'),
                    ];
                }),
            MorphMany::make(__('File Bank'), 'contents', 'App\Nova\FileBank'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
