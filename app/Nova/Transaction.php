<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;

class Transaction extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Transaction::class;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'حسابرسی';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['id',];

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label(): string
    {
        return __('Transaction');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Number::make(__('Cost'), 'amount')
                ->sortable()
                ->readonly(),
            Number::make(__('Factor Number'), 'factor_number')
                ->sortable()
                ->readonly(),
            BelongsTo::make(__('Cart'), 'cart', 'App\Nova\Cart')
                ->sortable()
                ->readonly(),
            BelongsTo::make(__('Discount'), 'discount', 'App\Nova\Discount')
                ->sortable()
                ->readonly(),
            Select::make(__('Status'), 'condition')->options([
                'PENDING' => __('Pending'),
                'SUCCESSFUL' => __('Successful'),
                'FAILED' => __('Failed'),
                'NOSERVICE' => __('NoService'),
            ])->displayUsingLabels()
                ->sortable()
                ->readonly(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
