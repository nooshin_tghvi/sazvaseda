<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;

class ContactUs extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static string $model = \App\Models\ContactUs::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'tracking_no';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['tracking_no',];

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label(): string
    {
        return __('Contact Us');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Full Name'), 'full_name')
                ->sortable()
                ->rules('required', 'persian_alpha', 'max:150'),
            Text::make(__('Email'), 'email')
                ->sortable()
                ->rules('required', 'email', 'max:150'),
            Text::make(__('Subject'), 'subject')
                ->sortable()
                ->rules('required', 'persian_alpha_eng_num', 'max:150'),
            Text::make(__('Description'), 'description')
                ->sortable()
                ->rules('required', 'persian_alpha_eng_num', 'max:150'),
            Text::make(__('Tracking Number'), 'tracking_no')
                ->sortable()
                ->rules('required', 'max:8'),
            DateTime::make(__('Opened at'), 'opened_at')->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
