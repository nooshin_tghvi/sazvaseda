<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

class Package extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Package::class;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'پکیج';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['id',];

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label(): string
    {
        return __('Package');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name')
                ->sortable()
                ->rules('required', 'persian_alpha_num	', 'max:120')
                ->creationRules('unique:packages,name')
                ->updateRules('unique:packages,name,{{resourceId}}'),
            BelongsTo::make(__('Manager'), 'user', 'App\Nova\User')
                ->sortable(),
            Number::make(__('Price'), 'price')
                ->sortable()
                ->rules('required', 'integer')
                ->default(0),
            Number::make(__('Delivery Time'), 'delivery_time')
                ->sortable()
                ->rules('required', 'integer')
                ->default(0),
            Image::make(__('Image'), 'image')
                ->disk('public')
                ->rules('required', 'max:2500', 'mimes:jpeg,png,gif,svg'),
            Textarea::make(__('Description'), 'description')
                ->nullable(),
            Number::make(__('PO Number'), 'po_number')
                ->sortable()
                ->readonly(),
            Boolean::make(__('Is Active'), 'is_active')
                ->default(False)
                ->help('کاربر مشخص می کند که استدیو فعال است یا خیر')
                ->sortable(),
            Boolean::make(__('Is Viewable'), 'is_viewable')
                ->default(False)
                ->help('ادمین مشخص می کند که استدیو فعال است یا خیر')
                ->sortable(),
            MorphMany::make(__('File Bank'), 'contents', 'App\Nova\FileBank'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
