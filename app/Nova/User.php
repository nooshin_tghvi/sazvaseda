<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Text;
use phpDocumentor\Reflection\Types\False_;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\User::class;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'اشخاص';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'mobile';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['mobile', 'email',];

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label(): string
    {
        return __('User');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'first_name')
                ->sortable()
                ->rules('required', 'persian_alpha', 'max:75'),
            Text::make(__('Last Name'), 'last_name')
                ->sortable()
                ->rules('required', 'persian_alpha', 'max:75'),
            Text::make(__('Email'), 'email')
                ->sortable()
                ->rules('required', 'email', 'max:150')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),
            Text::make(__('Mobile'), 'mobile')
                ->sortable()
                ->rules('required', 'ir_mobile:zero', 'max:11')
                ->creationRules('unique:users,mobile')
                ->updateRules('unique:users,mobile,{{resourceId}}'),
            Number::make(__('Stock'), 'stock')
                ->sortable()
                ->hideFromIndex()
                ->readonly(),
            Text::make(__('Melli Code'), 'melli_code')
                ->sortable()
                ->rules('ir_national_code', 'max:10')
                ->creationRules('unique:users,melli_code')
                ->updateRules('unique:users,melli_code,{{resourceId}}')
                ->hideFromIndex(),
            Text::make(__('IBAN'), 'iban')
                ->sortable()
                ->rules('ir_sheba')
                ->creationRules('unique:users,iban')
                ->updateRules('unique:users,iban,{{resourceId}}')
                ->hideFromIndex(),
            Boolean::make(__('Is Approved'), 'is_approved')
                ->default(True)
                ->sortable(),
            HasOne::make(__('Artist'), 'artist', 'App\Nova\Artist')
                ->sortable(),
            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
