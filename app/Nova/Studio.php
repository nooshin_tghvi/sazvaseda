<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

class Studio extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Studio::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['name',];

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label(): string
    {
        return __('Studio');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name')
                ->sortable()
                ->rules('required', 'persian_alpha_num', 'max:120'),
            BelongsTo::make(__('Owner'), 'user', 'App\Nova\User')
                ->sortable(),
            /*Avatar::make(__('Logo'), 'logo')
                ->disk('public')
                ->nullable()
                ->rules('max:255'),*/
            Textarea::make(__('Address'), 'address')
                ->rules('required'),
            Number::make(__('Price'), 'price')
                ->sortable()
                ->rules('required', 'integer')
                ->default(0),
            Boolean::make(__('Is Active'), 'is_active')
                ->default(False)
                ->sortable(),
            Select::make(__('Status'), 'status')->options([
                'PENDING' => __('Pending'),
                'APPROVAL' => __('Approval'),
                'FAILED' => __('Failed'),
            ])->displayUsingLabels()
                ->sortable()
                ->rules('required', Rule::in(['PENDING', 'APPROVAL', 'FAILED'])),
            BelongsTo::make(__('City'), 'City', 'App\Nova\City')
                ->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
