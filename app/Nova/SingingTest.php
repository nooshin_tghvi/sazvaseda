<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Textarea;

class SingingTest extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\SingingTest::class;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
//    public static $group = 'اطلاعات محتوایی';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['id',];

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label(): string
    {
        return __('Singing Test');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            BelongsTo::make(__('User'), 'user', 'App\Nova\User')
                ->sortable(),
            File::make(__('File'), 'file')
                ->disk('public')
                ->rules('max:10000', 'mimes:opus,ogg,3gp,wav,mp3')
                ->hideFromIndex(),
            Boolean::make(__('Has been answered'), 'has_been_answered')
                ->sortable(),
            Textarea::make(__('Description'), 'description')
                ->rules('max:21500')
                ->nullable(),
            Textarea::make(__('Response'), 'response')
                ->rules('max:21500')
                ->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
