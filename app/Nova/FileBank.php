<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

class FileBank extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\FileBank::class;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'اطلاعات محتوایی';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['name',];

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label(): string
    {
        return __('File Bank');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name')
                ->sortable()
                ->rules('required', 'max:75'),
            MorphTo::make(__('Owner'), 'owner')->types([
                Artist::class,
                Package::class,
            ]),
            Select::make(__('Type'), 'type')->options([
                'IMAGE' => __('Image'),
                'SOUND' => __('Sound'),
                'URL' => __('URL')
            ])->displayUsingLabels()
                ->sortable()
                ->rules('required', Rule::in(['IMAGE', 'SOUND', 'URL'])),
//            Boolean::make(__('Is Saleable'), 'is_saleable')
//                ->sortable(),
            Date::make(__('date'), 'date')
                ->sortable()
                ->rules('required', 'date'),
            Textarea::make(__('Description'), 'description')
                ->nullable(),
            File::make(__('Sound'), 'sound')
                ->disk('public')
                ->rules('max:5000', 'mimes:opus,ogg,3gp,wav,mp3')
                ->hideFromIndex(),
            Image::make(__('Image'), 'image')
                ->disk('public')
                ->rules('max:2000')
                ->hideFromIndex(),
            Text::make(__('URL'), 'url')
                ->sortable()
                ->rules('max:191'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
