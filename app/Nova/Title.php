<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

class Title extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Title::class;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'اطلاعات پایه';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['name',];

    /**
     * Get the displayable name of the dashboard.
     *
     * @return string
     */
    public static function label(): string
    {
        return __('Title');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name')
                ->rules('required', 'max:50')
                ->creationRules('unique:titles,name')
                ->updateRules('unique:titles,name,{{resourceId}}'),
            Textarea::make(__('Description'), 'description')
                ->nullable(),
            BelongsToMany::make(__('Artists'), 'artists', 'App\Nova\Artist')
                ->fields(function () {
                    return [
                        Boolean::make(__('Accept Order'), 'accept_order')
                            ->sortable(),
                        Number::make(__('Order Price'), 'order_price')
                            ->sortable()
                            ->rules('integer', 'min:0')
                            ->default(0),
                        Text::make(__('Description'), 'description')
                            ->nullable()
                            ->rules('max:191'),
                    ];
                }),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
