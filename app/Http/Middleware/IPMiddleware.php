<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class IPMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    private array $allowedIP = [
        '127.0.0.1',
    ];

    public function handle(Request $request, Closure $next)
    {
//        if (!in_array($request->ip(), $this->allowedIP)) {
            //log::notice('A: '.$request->ip());
//        }
        return $next($request);
        //log::notice('B: '. $request->ip());
//        return response(null, 403);
    }
}
