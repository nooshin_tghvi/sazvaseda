<?php

namespace App\Http\Controllers;

use App\Models\Discount;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class DiscountController extends Controller
{
    public function isValidCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|exists:discounts',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $discount = Discount::where('code', $request->input('code'))->first();

        $result = $this->checkingValidation($discount);
        if ($result['error']) {
            return response($result);
        }

        $result = $this->applyDiscountCode($discount, $result['cart']);
        return response([
            'error' => false,
            'messages' => [' کد تخفیف اعمال شد و مقدار ' . $result['amountOfDiscount'] . ' تومان از خرید شما کم شده است '],
            'amountOfDiscount' => $result['amountOfDiscount'],
            'amountOfPayment' => $result['amountOfPayment']
        ]);
    }

    public function checkingValidation($discount): array
    {
        $user = $this->user();
        $cart = $user->LastUnpaidCart;
        if ($cart->final_cost == 0) {
            return ['error' => true, 'messages' => ['سبد خرید شما خالی است']];
        }

        $current = Carbon::today('Asia/Tehran');
        if ($current->gt($discount->expire_date)) {
            return ['error' => true, 'messages' => ['تاریخ استفاده از کد مورد نظر به پایان رسیده است']];
        }
        if ($discount->count == $discount->used_number) {
            return ['error' => true, 'messages' => ['تعداد افراد استفاده کننده از این کد تخفیف به حداکثر رسیده است']];
        }

        return ['error' => false, 'cart' => $cart];
    }

    public function applyDiscountCode($discount, $cart): array
    {
        if ($discount->type === 'PERCENT') {
            $amountOfDiscount = $cart->final_cost * $discount->value / 100;
            if ($amountOfDiscount > $discount->maximum_value) {
                $amountOfDiscount = $discount->maximum_value;
            }
            $amountOfPayment = $cart->final_cost - $amountOfDiscount;
        } else { //type is 'CASH'
            if ($cart->final_cost > $discount->value) {
                $amountOfDiscount = $discount->value;
                $amountOfPayment = $cart->final_cost - $amountOfDiscount;
            } else {
                $amountOfDiscount = $cart->final_cost;
                $t = $cart->final_cost - $amountOfDiscount;
                $amountOfPayment = ($t > 0) ? $t : 0;
            }
        }
        return [
            'amountOfDiscount' => (int)$amountOfDiscount,
            'amountOfPayment' => (int)$amountOfPayment
        ];
    }
}
