<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\County;
use App\Models\Province;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function getProvinces(Request $request)
    {
        $provinces = Province::all();

        $rpp = $request->filled('rpp')
            ? $request->input('rpp')
            : 10;

        if ($request->filled('column') && $request->filled('isDesc'))
            $provinces = $provinces->sortBy($request->input('column'), SORT_REGULAR, $request->input('isDesc'));

        if ($request->filled('search')) {
            $name = $request->input('search');
            $provinces = $provinces->filter(function ($item) use ($name) {
                return strpos($item->name, $name) !== false;
            });
        }

        $provinces = $this->pagination($request, $provinces, $rpp);

        return response(['error' => false, 'provinces' => $provinces]);
    }

    public function getCounties(Request $request)
    {
        $counties = County::all();

        $rpp = $request->filled('rpp')
            ? $request->input('rpp')
            : 10;

        if ($request->filled('column') && $request->filled('isDesc'))
            $counties = $counties->sortBy($request->input('column'), SORT_REGULAR, $request->input('isDesc'));

        if ($request->filled('search')) {
            $name = $request->input('search');
            $counties = $counties->filter(function ($item) use ($name) {
                return strpos($item->name, $name) !== false;
            });
        }

        if ($request->filled('provinceIds'))
            $counties = $counties->whereIn('province_id', explode('_', $request->input('provinceIds')));

        $counties = $this->pagination($request, $counties, $rpp);

        return response(['error' => false, 'counties' => $counties]);
    }

    public function getCities(Request $request)
    {
        $cities = City::all();

        $rpp = $request->filled('rpp')
            ? $request->input('rpp')
            : 10;

        if ($request->filled('column') && $request->filled('isDesc'))
            $cities = $cities->sortBy($request->input('column'), SORT_REGULAR, $request->input('isDesc'));

        if ($request->filled('search')) {
            $name = $request->input('search');
            $cities = $cities->filter(function ($item) use ($name) {
                return strpos($item->name, $name) !== false;
            });
        }

        if ($request->filled('provinceIds'))
            $cities = $cities->whereIn('province_id', explode('_', $request->input('provinceIds')));

        if ($request->filled('countyIds'))
            $cities = $cities->whereIn('county_id', explode('_', $request->input('countyIds')));

        $cities = $this->pagination($request, $cities, $rpp);

        return response(['error' => false, 'cities' => $cities]);
    }

    public function detail($cityId)
    {
        $city = City::where('id', $cityId)->first();
        if (is_null($city))
            return response(['error' => true, 'messages' => ['شهر مورد نظر پیدا نشد']]);

        $studio['province'] = Province::find($city->province_id)->name;
        $studio['county'] = County::find($city->county_id)->name;
        $studio['city'] = $city->name;
        $studioController = new StudioController();
        $studio['studios'] = $studioController->assignStudios($city->studios);

        return response(['error' => false, 'studio' => $studio]);
    }

    public function assignCities($cities): array
    {
        $data = [];
        foreach ($cities as $city)
            array_push($data, [
                'city_id' => $city->id,
                'province' => Province::find($city->province_id)->name,
                'county' => County::find($city->county_id)->name,
//                'link' => (new CreateLinkController)->__invoke('city', $city->id),
                'city' => $city->name,
            ]);

        return $data;

    }
}
