<?php

namespace App\Http\Controllers;

use App\Models\Title;
use App\Models\TitleArtist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class TitleController extends Controller
{
    public function get(Request $request)
    {
        $titles = Title::query()
            ->get()
            ->map(function (Title $title) {
                $title->link = (new CreateLinkController)->__invoke('title', $title->id);
                return $title;
            });;

        $rpp = $request->filled('rpp')
            ? $request->input('rpp')
            : 10;

        if ($request->filled('column') && $request->filled('isDesc'))
            $titles = $titles->sortBy($request->input('column'), SORT_REGULAR, $request->input('isDesc'));

        if ($request->filled('search')) {
            $name = $request->input('search');
            $titles = $titles->filter(function ($item) use ($name) {
                return strpos($item->name, $name) !== false;
            });
        }

        $titles = $this->pagination($request, $titles, $rpp);

        return response(['error' => false, 'titles' => $titles]);
    }

    public function detail($titleId)
    {
        if (!Title::query()->where('id', $titleId)->exists())
            return response(['error' => true, 'messages' => ['یافت نشد']]);

        $title = Title::find($titleId);
        $data = $title;

        $artistsController = new ArtistController();
        $data['artists'] = $artistsController->assignArtists($title->artists);

        $packageController = new PackageController();
        $data['pckgs'] = $packageController->assignPackages($title->packages);

        return response(['error' => false, 'data' => $data]);
    }

    public function assignTitles($titles): array
    {
        $data = [];
        foreach ($titles as $title)
            array_push($data, [
                'id' => $title->id,
                'name' => $title->name,
                'pivot' => $title->pivot,
//                'pivot' => $title->pivot->makeHidden(['artist_id','title_id']),
//                'link' => (new CreateLinkController)->__invoke('title', $title->id),
            ]);

        return $data;
    }

    public function myTitles()
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $artist = $this->artist();
        $titles = $artist->titles;

        return response(['error' => false, 'titles' => $titles]);
    }

    public function addTitleForArtist(Request $request)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $validator = Validator::make($request->all(), [
            'title_id' => 'required|exists:titles,id',
            'description' => 'nullable|string|max:191',
            'accept_order' => 'required|boolean',
            'order_price' => 'required|integer|min:0',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $artist = $this->artist();
        $titleId = $request->input('title_id');
        if (TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->exists())
            return response(['error' => true, 'messages' => ['مهارت انتخابی ثبت شده است']]);

        $newTitle = TitleArtist::create([
            'artist_id' => $artist->id,
            'title_id' => $titleId,
            'description' => $request->input('description'),
            'accept_order' => $request->input('accept_order'),
            'order_price' => $request->input('accept_order') ? $request->input('order_price') : 0,
        ]);
        return response(['error' => false, 'new_title' => $newTitle]);
    }

    public function deleteTitleFromArtist($titleId)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $artist = $this->artist();
        if (TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->exists()) {
            TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->delete();
            return response(['error' => false, 'messages' => ['مهارت مورد نظر حذف شد']]);
        } else
            return response(['error' => true, 'messages' => ['مهارت مورد نظر وجود ندارد']]);
    }

    public function editTitleForArtist(Request $request, $titleId)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $validator = Validator::make($request->all(), [
            'description' => 'nullable|string|max:191',
            'accept_order' => 'required|boolean',
            'order_price' => 'required|integer|min:0',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $artist = $this->artist();

        if (!TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->exists())
            return response(['error' => true, 'messages' => ['مهارت انتخابی در دسترس شما نیست']]);

        $ta = TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->first();
        $ta->description = $request->input('description');
        $ta->accept_order = $request->input('accept_order');
        $ta->order_price = $request->input('order_price');
        $ta->save();
        $ta['title_name'] = Title::where('id', $titleId)->first('name')->name;
        return response(['error' => false, 'title' => $ta]);
    }

    public function editDescription(Request $request, $titleId)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $validator = Validator::make($request->all(), [
            'description' => 'string|max:191'
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $artist = $this->artist();
        if (TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->exists()) {
            $ta = TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->first();
            $ta->description = $request->input('description');
            $ta->save();
            return response(['error' => false, 'description' => $ta->description]);
        } else
            return response(null, 406);

    }

    public function changeAcceptOrder($titleId)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $artist = $this->artist();
        if (TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->exists()) {
            $ta = TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->first();
            $ta->accept_order = !$ta->accept_order;
            $ta->save();
            return response(['error' => false, 'accept_order' => $ta->accept_order]);
        } else
            return response(null, 406);
    }

    public function editOrderPrise(Request $request, $titleId)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $validator = Validator::make($request->all(), [
            'order_price' => 'required|integer|min:0',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $artist = $this->artist();
        if (TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->exists()) {
            $ta = TitleArtist::where('artist_id', $artist->id)->where('title_id', $titleId)->first();
            if (!$ta->accept_order)
                return response(['error' => true, 'messages' => ['ابتدا گزینه پذیرش سفارش را قبول کنید']]);
            $ta->order_price = $request->input('order_price');
            $ta->save();
            return response(['error' => false, 'order_price' => $ta->order_price]);
        } else
            return response(null, 406);
    }

    public function existTitleArtist($id)
    {
        return TitleArtist::where('id', $id)->exists();
    }

    public function getTitleArtist($id)
    {
        return TitleArtist::find($id);
    }
}
