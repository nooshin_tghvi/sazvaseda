<?php


namespace App\Http\Controllers;


use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Throwable;

class PaypingController extends Controller
{

    const BASE_URL = 'https://api.payping.ir/v1/';

    private string $token;
    private Client $restCall; //کتابخانه guzzle برای فرخوانی وب سرویس ها
    private array $body;
    private string $payUrl; //آدرس ساخته شده توسط payping برای هدایت کاربر به درگاه

    public function __construct()
    {
        $this->token = config('payping.token');
        $headers = [
            'Content-type' => 'application/json; charset=utf-8',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
        ];

        $this->restCall = new Client([
            'base_uri' => self::BASE_URL,
            'headers' => $headers
        ]);
    }

    /**
     * @param $fullName
     * @param $finalCost
     * @param $mobile
     * @param $factorNumber
     */
    public function createBodyForPay($fullName, $finalCost, $mobile, $factorNumber)
    {
        unset ($this->body);
        $this->body['amount'] = $finalCost;
        $this->body['payerName'] = $fullName;
        $this->body['payerIdentity'] = $mobile;
        $this->body['clientRefId'] = $factorNumber;
        $this->body['description'] = 'خرید از سایت' . config('app.url');
        $this->body['returnUrl'] = config('app.url') . '/api/pay/verify/' . $factorNumber;
    }

    public function pay(): array
    {
        try {
            $result = $this->restCall->post(self::BASE_URL . 'pay', ['body' => json_encode($this->body)]);
            $result = json_decode($result->getBody()->getContents(), false);
            if (!isset($result->code))
                return ['error' => true, 'messages' => ['error in get code from payping']];
            return ['error' => false, 'code' => $result->code];
        } catch (Throwable $e) {
            log::info('error.1) ' . $e);
        }
        return ['error' => true, 'messages' => ['مجددا تلاش کنید']];
    }

    public function setPayUrl($code)
    {
        $this->payUrl = self::BASE_URL . 'pay/gotoipg/' . $code;
    }

    public function getPayUrl(): string
    {
        return $this->payUrl;
    }

    public function createBodyForVerify($refId, $finalCost)
    {
        unset ($this->body);
        $this->body['refId'] = $refId;
        $this->body['amount'] = $finalCost;
    }

    public function verify(): array
    {
        try {
            $result = $this->restCall->post(self::BASE_URL . 'pay/verify', ['body' => json_encode($this->body)]);
            if ($result->getStatusCode() >= 200 && $result->getStatusCode() < 300)
                return ['error' => false, 'done' => true, 'cardNumber' => $result['cardNumber']];
            else
                return ['error' => true, 'messages' => ['پول شما تا 72 ساعت آینده به حساب شما برگشت داده می شود']];

        } catch (RequestException $re) {
            log::info('error.2) ' . $re->getResponse()->getBody()->getContents() . ' / ' .
                $re->getResponse()->getStatusCode() . ' / ' .
                $re->getPrevious());
        } catch (Exception $e) {
            log::info('error.3) ' . $e->getMessage() . ' / ' . $e->getCode() . ' / ' . $e->getPrevious());
        } catch (Throwable $e) {
            log::info('error.4) ' . $e);
        }
        return ['error' => true, 'messages' =>
            ['پول شما تا 72 ساعت آینده به حساب شما برگشت داده می شود ']];
    }
}
