<?php

namespace App\Http\Controllers;

use App\Models\FileBank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PortfolioController extends Controller
{
    protected string $ownerType;
    protected string $ownerId;
    protected string $pathImage;
    protected string $pathSound;

    protected function detail($itemId)
    {
        return FileBank::where('id', $itemId)->first();
    }

    private function validation(Request $request): array
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:75',
            'date' => 'required|date',
//            'is_saleable' => 'nullable|boolean',
            'description' => 'required|string|max:21500', //21500 char,
            'type' => ['required', 'string', Rule::in(['image', 'sound', 'url'])],
            'image' => ['image', 'mimes:jpeg,png,gif,svg', 'max:2000',           //2000 KB
                Rule::requiredIf($request->input('type') == 'image')],
            'sound' => ['file', 'mimes:opus,ogg,3gp,wav,mp3', 'max:10000',      //10000 KB
                Rule::requiredIf($request->input('type') == 'sound')],
            'url' => ['nullable', 'string', 'max:191',                          //191 char
                Rule::requiredIf($request->input('type' == 'url'))],
        ]);
        if ($validator->fails()) {
            return ['error' => true, 'messages' => $validator->errors()->all()];
        } else
            return ['error' => false];
    }

    protected function register(Request $request)
    {
        $result = $this->validation($request);
        if ($result['error']) {
            return response($result);
        }

        $fb = FileBank::create([
            'owner_type' => $this->ownerType,
            'owner_id' => $this->ownerId,
            'name' => $request->input('name'),
            'date' => $request->input('date'),
//            'is_saleable' => $request->input('is_saleable'),
            'description' => $request->input('description'),
            'type' => strtoupper($request->input('type')),
            'url' => $request->input('url')
        ]);

        if ($request->hasFile('image')) {
            $fb->image = $this->saveAndGetPathFile($this->pathImage, $request->file('image'));
            $fb->save();
        }

        if ($request->hasFile('sound')) {
            $fb->sound = $this->saveAndGetPathFile($this->pathSound, $request->file('sound'));
            $fb->save();
        }
        return response(['error' => false, 'portfolio' => $this->putImageURLInObjects([$fb], ['image', 'sound'])]);
    }

    protected function delete($id)
    {
        FileBank::find($id)->delete();
        return response(['error' => false, 'messages' => ['نمونه کار مورد نظر با موفقیت حذف شد']]);
    }

    protected function edit(Request $request, $id)
    {
        $result = $this->validation($request);
        if ($result['error']) {
            return response($result);
        }

        $fb = FileBank::find($id);
        $fb->name = $request->input('name');
        $fb->date = $request->input('date');
//        $fb->description = $request->input('is_saleable');
        $fb->description = $request->input('description');
        $fb->save();
        $fb = $this->deleteOldFileAndSaveFile($request, $fb);

        return response(['error' => false, 'portfolio' => $this->putImageURLInObjects([$fb], ['image', 'sound'])]);
    }

    protected function changeName(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:75',

        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $fb = FileBank::find($id);
        $fb->name = $request->input('name');
        $fb->save();
        return response(['error' => false, 'name' => $fb->name]);
    }

    protected function changeDate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'date' => $request->input('date'),

        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $fb = FileBank::find($id);
        $fb->date = $request->input('date');
        $fb->save();
        return response(['error' => false, 'date' => $fb->date]);
    }

    protected function changeSaleability($id)
    {
        $fb = FileBank::find($id);
        $fb->is_saleable = !$fb->is_saleable;
        $fb->save();
        return response(['error' => false, 'is_saleable' => $fb->is_saleable]);
    }

    protected function changeDescription(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required|string|max:21500', //21500 char,

        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $fb = FileBank::find($id);
        $fb->description = $request->input('description');
        $fb->save();
        return response(['error' => false, 'description' => $fb->description]);
    }

    protected function changeFiles(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required', 'string', Rule::in(['image', 'sound', 'url'])],
            'image' => ['image', 'mimes:jpeg,png,gif,svg', 'max:2000',           //2000 KB
                Rule::requiredIf($request->input('type') == 'image')],
            'sound' => ['file', 'mimes:opus,ogg,3gp,wav,mp3', 'max:10000',      //10000 KB
                Rule::requiredIf($request->input('type') == 'sound')],
            'url' => ['nullable', 'string', 'max:191',                          //191 char
                Rule::requiredIf($request->input('type' == 'url'))],
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $fb = FileBank::find($id);
        $fb = $this->deleteOldFileAndSaveFile($request, $fb);

        return response(['error' => false, 'portfolio' => $this->putImageURLInObjects([$fb], ['image', 'sound'])]);
    }

    private function deleteOldFileAndSaveFile(Request $request, $fb)
    {
        if ($request->hasFile('image')) {
            if (!is_null($fb->image))
                $this->deleteFile($fb->image);
            $fb->image = $this->saveAndGetPathFile($this->pathImage, $request->file('image'));
            $fb->type = strtoupper($request->input('type'));
            $fb->save();
        }

        if ($request->hasFile('sound')) {
            if (!is_null($fb->sound))
                $this->deleteFile($fb->sound);
            $fb->sound = $this->saveAndGetPathFile($this->pathSound, $request->file('sound'));
            $fb->type = strtoupper($request->input('type'));
            $fb->save();
        }

        if ($request->has('url')) {
            $fb->url = $request->input('url');
            $fb->type = strtoupper($request->input('type'));
            $fb->save();
        }
        return $fb;
    }
}

