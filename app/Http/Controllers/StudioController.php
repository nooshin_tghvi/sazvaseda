<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Studio;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class StudioController extends Controller
{
    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     */
    public function get(Request $request)
    {
        $studios = Studio::query()
            ->where('status', 'APPROVAL')
            ->get()
            ->map(function (Studio $studio) {
                $studio->link = (new CreateLinkController)->__invoke('studio', $studio->id);
                $cityController = new CityController();
                $studio->geographical_information = $cityController->assignCities([$studio->city])[0];
//                $studio->logo = $this->createURLForImage($studio->logo);
                $studio->pictures = $this->addImagesOfStudios([$studio])[0];
                return $studio;
            });

        $rpp = $request->filled('rpp')
            ? $request->input('rpp')
            : 10;

        if ($request->filled('search')) {
            $name = $request->input('search');
            $artists = $studios->filter(function ($item) use ($name) {
                return strpos($item->name, $name) !== false || strpos($item->address, $name) !== false;
            });
        }

        if ($request->filled('column') && $request->filled('isDesc'))
            $studios = $studios->sortBy($request->input('column'), SORT_REGULAR, $request->input('isDesc'));

        if ($request->filled('cityIds'))
            $studios = $studios->whereIn('city_id', explode('_', $request->input('cityIds')));

        if ($request->filled('min_max') && sizeof(explode('_', $request->input('min_max'))) == 2) {
            $min_max = explode('_', $request->input('min_max'));
            $studios = $studios->whereBetween('price', [$min_max[0], $min_max[1]]);
        }

        $studios = $this->pagination($request, $studios, $rpp);

        return response(['error' => false, 'studios' => $studios,
            'price_max' => Studio::max('price'), 'price_min' => Studio::min('price')]);
    }

    private function addImagesOfStudios($studios): array
    {
        $images = [];
        foreach ($studios as $studio)
            array_push($images, $this->putImageURLInObjects($studio->images, ['path']));
        return $images;
    }

    public function detail($studioId)
    {
        $studio = $this->findAllowedStudioToShow($studioId);
        if (is_null($studio))
            return response(['error' => true, 'messages' => ['استدیو مورد نظر پیدا نشد']]);

        $studio = $studio->makeVisible('is_active');
        $cityController = new CityController();
        $studio->geographical_information = $cityController->assignCities([$studio->city])[0];
        $studio->pictures = $this->addImagesOfStudios([$studio])[0];

        return response(['error' => false, 'studio' => $studio]);
    }

    public function assignStudios($studios, $beApproved = true): array
    {
        $data = [];
        foreach ($studios as $studio) {
            if ($beApproved) {
                if ($studio->status != 'APPROVAL')
                    continue;
            }
            array_push($data, [
                'id' => $studio->id,
                'name' => $studio->name,
//                'logo' => $this->createURLForImage($studio->logo),
//                'link' => (new CreateLinkController)->__invoke('studio', $studio->id),
            ]);
        }
        return $data;
    }

    public function findAllowedStudioToShow($id)
    {
        return Studio::where('id', $id)
            ->where('status', 'APPROVAL')
            ->first();
    }

    public function detailsForOwner($studioId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        $studio = $this->findStudioById($studioId);
        $studio = $studio->makeVisible(['is_active', 'status']);

        $cityController = new CityController();
        $studio->geographical_information = $cityController->assignCities([$studio->city])[0];
        $studio->pictures = $this->addImagesOfStudios([$studio])[0];

        return response(['error' => false, 'studio' => $studio]);
    }

    private function isForUser($studioId, $userId)
    {
        return Studio::where('id', $studioId)->where('user_id', $userId)->exists();
    }

    private function findStudioById($id)
    {
        return Studio::find($id);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|persian_alpha_num|max:120|unique:studios',
//            'logo' => 'required|image|mimes:jpeg,png,gif,svg|max:512', //512 KB
            'city_id' => 'required|exists:cities,id',
            'address' => 'required|string|max:191',
            'price' => 'required|integer|min:0',
            'description' => 'nullable|string|max:21500', //21500 char'
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

//        $path = 'image/studio/logo/';
//        $tmp = $this->saveAndGetPathFile($path, $request->file('logo'));

        $studio = Studio::create([
            'name' => $request->input('name'),
//            'logo' => $tmp,
            'city_id' => $request->input('city_id'),
            'address' => $request->input('address'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
            'user_id' => $this->user()->id,

//            'name' => 'nullable|persian_alpha_num|max:75',
//            'images.*' => 'image|mimes:jpeg,png,gif,svg|max:512', //512 KB
//            'description' => 'nullable|string|max:191',
        ]);

        $cityController = new CityController();
        $studio->geographical_information = $cityController->assignCities([$studio->city])[0];

        $this->sendNotice('NEW_STUDIO) id:' . $studio->id);

//        return response(['error' => false, 'studio' => $this->putImageURLInObjects([$studio], ['logo'])[0]]);
        return response(['error' => false, 'studio' => $studio]);
    }

    public function delete($studioId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);
        Studio::find($studioId)->delete();
        return response(['error' => false, 'messages' => ['استدیو مورد نظر حذف شد']]);
    }

    public function myStudios(Request $request)
    {
        $studios = $this->user()->studios;

        if ($request->filled('column') && $request->filled('isDesc'))
            $studios = $studios->sortBy($request->input('column'), SORT_REGULAR, $request->input('isDesc'));

        $studios->makeVisible(['is_active', 'status']);
        $studios->makeHidden('user_id');

        foreach ($studios as $studio) {
            $cityController = new CityController();
            $studio->geographical_information = $cityController->assignCities([$studio->city])[0];
            $studio->pictures = $this->addImagesOfStudios([$studio])[0];
        }

        return response(['error' => false, 'studios' => $studios]);
    }

    public function changeName(Request $request, $studioId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        $studio = $this->findStudioById($studioId);

        $validator = Validator::make($request->all(), [
            'name' => [
                'required', 'persian_alpha_num', 'max:120',
                Rule::unique('studios')->ignore($studio->id),
            ]
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        if ($studio->name != $request->input('name')) {
            $studio->name = $request->input('name');
            $studio->save();
        }
        return response(['error' => false, 'name' => $studio->name]);
    }

    public function changeLogo(Request $request, $studioId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        $studio = $this->findStudioById($studioId);

        $validator = Validator::make($request->all(), [
            'logo' => 'required|image|mimes:jpeg,png,gif,svg|max:512', //512 KB
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $this->deleteFile($studio->logo);
        $path = 'image/studio/logo';
        $studio->logo = $this->saveAndGetPathFile($path, $request->file('logo'));
        $studio->save();

        return response(['error' => false, 'studio' => $this->putImageURLInObjects([$studio], ['logo'])[0]]);
    }

    public function changeCity(Request $request, $studioId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        $validator = Validator::make($request->all(), [
            'city_id' => 'required|exists:cities,id',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $studio = $this->findStudioById($studioId);
        if ($studio->city_id != $request->input('city_id')) {
            $studio->city_id = $request->input('city_id');
            $studio->save();
        }
        return response(['error' => false, 'city_id' => $studio->city_id]);
    }

    public function changeAddress(Request $request, $studioId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        $validator = Validator::make($request->all(), [
            'address' => 'required|string|max:191',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $studio = $this->findStudioById($studioId);
        if ($studio->address != $request->input('address')) {
            $studio->address = $request->input('address');
            $studio->save();
        }
        return response(['error' => false, 'address' => $studio->address]);
    }

    public function changePrice(Request $request, $studioId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        $validator = Validator::make($request->all(), [
            'price' => 'required|integer|min:0',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $studio = $this->findStudioById($studioId);
        if ($studio->price != $request->input('price')) {
            $studio->price = $request->input('price');
            $studio->save();
        }
        return response(['error' => false, 'price' => $studio->price]);
    }

    public function changeDescription(Request $request, $studioId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        $validator = Validator::make($request->all(), [
            'description' => 'nullable|string|max:21500', //21500 char
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $studio = $this->findStudioById($studioId);
        if ($studio->description != $request->input('description')) {
            $studio->description = $request->input('description');
            $studio->save();
        }
        return response(['error' => false, 'description' => $studio->description]);
    }

    public function changeActivation($studioId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        $studio = $this->findStudioById($studioId);

        $studio->is_active = !$studio->is_active;
        $studio->save();

        return response(['error' => false, 'is_active' => $studio->is_active]);
    }

    public function findAllowedStudioToReserve($id)
    {
        return Studio::where('id', $id)
            ->where('status', 'APPROVAL')
            ->where('is_active', true)
            ->first();
    }

    public function addImage(Request $request, $studioId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        $validator = Validator::make($request->all(), [
            'name' => 'nullable|persian_alpha_num|max:75',
            'image' => 'required|image|mimes:jpeg,png,gif,svg|max:512', //512 KB
            'description' => 'nullable|string|max:191',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $path = 'image/studio/';
        $tmp = $this->saveAndGetPathFile($path, $request->file('image'));

        $image = Image::create([
            'name' => $request->input('name'),
            'owner_type' => 'App\Models\Studio',
            'owner_id' => $studioId,
            'description' => $request->input('description'),
            'path' => $tmp,
        ]);
        $image = $image->makeVisible('description');
        $image = $this->putImageURLInObjects([$image], ['path']);
        return response(['error' => false, 'image' => $image]);
    }

    public function deleteImage($studioId, $imageId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        if (!$this->isImageForStudio($studioId, $imageId))
            return response(['error' => true, 'messages' => [__('site.selected_wrong_image_for_studio')]]);

        $image = $this->getImageOfStudio($imageId);
        $image->delete();

        return response(null, 204);
    }

    private function isImageForStudio($studioId, $imageId)
    {
        return Image::where('id', $imageId)
            ->where('owner_type', 'App\Models\Studio')
            ->where('owner_id', $studioId)
            ->exists();
    }

    private function getImageOfStudio($imageId)
    {
        return Image::find($imageId);
    }

    public function changeImageName(Request $request, $studioId, $imageId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        if (!$this->isImageForStudio($studioId, $imageId))
            return response(['error' => true, 'messages' => [__('site.selected_wrong_image_for_studio')]]);

        $validator = Validator::make($request->all(), [
            'name' => 'required|persian_alpha_num|max:75',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $image = $this->getImageOfStudio($imageId);
        if ($image->name != $request->input('name')) {
            $image->name = $request->input('name');
            $image->save();
        }
        return response(['error' => false, 'name' => $image->name]);
    }

    public function changeImageDescription(Request $request, $studioId, $imageId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        if (!$this->isImageForStudio($studioId, $imageId))
            return response(['error' => true, 'messages' => [__('site.selected_wrong_image_for_studio')]]);

        $validator = Validator::make($request->all(), [
            'description' => 'nullable|string|max:191',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $image = $this->getImageOfStudio($imageId);
        if ($image->description != $request->input('description')) {
            $image->description = $request->input('description');
            $image->save();
        }
        return response(['error' => false, 'description' => $image->description]);
    }

    public function changeImageFile(Request $request, $studioId, $imageId)
    {
        $user = $this->user();
        if (!$this->isForUser($studioId, $user->id))
            return response(['error' => true, 'messages' => [__('site.select_wrong_studio')]]);

        if (!$this->isImageForStudio($studioId, $imageId))
            return response(['error' => true, 'messages' => [__('site.selected_wrong_image_for_studio')]]);

        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,gif,svg|max:512', //512 KB
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $image = $this->getImageOfStudio($imageId);
        $path = 'image/studio/';
        $this->deleteFile($image->path);
        $image->path = $this->saveAndGetPathFile($path, $request->file('image'));
        $image->save();

        return response(['error' => false, 'image' => $this->putImageURLInObjects([$image], ['path'])]);
    }
}
