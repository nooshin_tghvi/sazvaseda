<?php

namespace App\Http\Controllers;


use App\Jobs\EvaluationToClearMobileCodesJob;
use App\Models\SMSVerify;
use Carbon\Carbon;
use IPPanel\Client;
use IPPanel\Errors\Error;
use IPPanel\Errors\HttpException;
use IPPanel\Errors\ResponseCodes;

class SMSController extends Controller
{
    private string $token;
    private Client $client;

    public function __construct()
    {
        $this->token = config('faraz-sms.token');
        $this->client = new Client($this->token);
    }

    public function sendSMSWithFaraz($mobile, $code): array
    {
        try {
            $bulkID = $this->client->send(
                config('faraz-sms.originator'),             // originator
                [$mobile],                                      // recipients
                'کاربر عزیز کد شما ' . $code . ' می باشد'         // message
            );

        } catch (Error $e) { // IPPanel error
            if ($e->code() == ResponseCodes::ErrUnprocessableEntity)
                $this->logMsg('SMSController, CreatePattern: 1) ' . "Error in input");
            else {
                $this->logMsg('SMSController, CreatePattern: 1) ' . $e->unwrap()); // get real content of error
                $this->logMsg('SMSController, CreatePattern: 2) ' . $e->getCode());
            }

        } catch (HttpException $e) { // http error
            $this->logMsg('SMSController, CreatePattern: 3) ' . $e->getMessage()); //get stringified error
            $this->logMsg('SMSController, CreatePattern: 4) ' . $e->getCode());
        }

        if (empty($bulkID)) {
            return ['error' => true];
        }
        return ['error' => false, 'bulkId' => $bulkID];
    }

    public function sendCodeToUser($mobile): array
    {
        $code = $this->getNumber(6);

        $smsVerify = SMSVerify::firstOrCreate(['mobile' => $mobile], ['mobile' => $mobile]);

        $result = $this->sendSMSWithFaraz($mobile, $code);
        if ($result['error'])
            return ['error' => true, 'messages' => ['لطفا دقایقی دیگر تلاش کنید']];

        $smsVerify->code = $code;
        $smsVerify->approved = false;
        $smsVerify->expiration_date = Carbon::now('Asia/Tehran')->addMinutes(2);
        $smsVerify->save();

        EvaluationToClearMobileCodesJob::dispatch($smsVerify)
            ->delay(now()->addMinutes(2));

        return ['error' => false, 'code' => $smsVerify->code]; //todo check delete code
    }

    public function verifyCode($mobile, $code): array
    {
        $smsVerify = SMSVerify::where('mobile', $mobile)->first();
        if (is_null($smsVerify))
            return ['error' => true, 'messages' => ['زمان دریافت کد گذشته است لطفا مجددا تلاش کنید']];
        if ($smsVerify->code != $code)
            return ['error' => true, 'messages' => ['کد اشتباه است. لطفا مجددا تلاش کنید']];

        $smsVerify->approved = true;
        $smsVerify->code = null;
        $smsVerify->expiration_date = Carbon::now('Asia/Tehran')->addMinutes(10);
        $smsVerify->save();

        EvaluationToClearMobileCodesJob::dispatch($smsVerify)
            ->delay(now()->addMinutes(10));

        return ['error' => false, 'messages' => ['شماره ی همراه شما تایید شد']];
    }

    public function isVerifiedMobile($mobile): bool
    {
        $smsVerify = SMSVerify::where('mobile', $mobile)
            ->where('approved', true)
            ->first();
        return !is_null($smsVerify);
    }
}
