<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reservation\GetDetailsAdvisorRequest;
use App\Http\Requests\Reservation\GetDetailsStudioRequest;
use App\Http\Requests\Reservation\SearchAdvisorRequest;
use App\Http\Requests\Reservation\SearchStudioRequest;
use App\Models\AllowedHour;
use App\Models\Artist;
use App\Models\ReservationDate;
use App\Models\ReservationDetail;
use App\Models\Studio;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use JsonException;
use Morilog\Jalali\Jalalian;

class ReservationController extends Controller
{
    public function getAllowedHours(): array
    {
        $allowedHours = AllowedHour::query()->get();
        $data = [];
        foreach ($allowedHours as $allowedHour) {
            $data [] = $this->wrappingAllowedHours($allowedHour);
        }
        return ['error' => false, 'allowed_hours' => $data];
    }

    public function searchInAdviser(SearchAdvisorRequest $request): array
    {
        return $this->search($request->get('date'), $request->get('id'), Artist::class);
    }

    public function searchInStudio(SearchStudioRequest $request): array
    {
        return $this->search($request->get('date'), $request->get('id'), Studio::class);
    }

    private function search($date, $id, $type): array
    {
        $mDate = Jalalian::fromFormat('Y-m-d', $date)
            ->toCarbon()
            ->format('Y-m-d');

        $result = ReservationDate::query()
            ->whereDate('date', $mDate)
            ->where('owner_id', $id)
            ->where('owner_type', $type)
            ->get();

        $result = $this->findReservationDetail($result);
        return ['error' => false, 'dates' => $result];
    }

    private function getDetailsOfReservation($id, $type, $fromToday)
    {
        $resDates = ReservationDate::query()
            ->where('owner_id', $id)
            ->where('owner_type', $type)
            ->whereDate('date', $fromToday ? '>=' : '<', Carbon::now()->format('Y-m-d'))
            ->orderByDesc('date')
            ->get();

        return $this->findReservationDetail($resDates);
    }

    public function getReservationForAdvisor(GetDetailsAdvisorRequest $request): array
    {
        $result = $this->getDetailsOfReservation($request->get('id'), Artist::class, true);
        $resDates = $this->pagination($request, $result, 7);
        return ['error' => false, 'dates' => $resDates];
    }

    public function getPastReservationForAdvisor(GetDetailsAdvisorRequest $request): array
    {
        $result = $this->getDetailsOfReservation($request->get('id'), Artist::class, false);
        $resDates = $this->pagination($request, $result, 7);
        return ['error' => false, 'dates' => $resDates];
    }

    public function getReservationForStudio(GetDetailsStudioRequest $request): array
    {
        $result = $this->getDetailsOfReservation($request->get('id'), Studio::class, true);
        $resDates = $this->pagination($request, $result, 7);
        return ['error' => false, 'dates' => $resDates];
    }

    public function getPastReservationForStudio(GetDetailsStudioRequest $request): array
    {
        $result = $this->getDetailsOfReservation($request->get('id'), Studio::class, false);
        $resDates = $this->pagination($request, $result, 7);
        return ['error' => false, 'dates' => $resDates];
    }

    public function addTimeForAdvisor(Request $request): array
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date|date_format:Y-m-d',
            'time' => 'required',
        ]);
        if ($validator->fails()) {
            return ['error' => true, 'messages' => $validator->errors()->all()];
        }

        return $this->changeTime($request, 'advisor', 'add');
    }

    public function removeTimeForAdvisor(Request $request): array
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date|date_format:Y-m-d',
            'time' => 'required',
        ]);
        if ($validator->fails()) {
            return ['error' => true, 'messages' => $validator->errors()->all()];
        }

        return $this->changeTime($request, 'advisor', 'delete');
    }

    public function addTimeForStudio(Request $request): array
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date|date_format:Y-m-d',
            'time' => 'required',
            'studioId' => 'required|exists:studios,id'
        ]);
        if ($validator->fails()) {
            return ['error' => true, 'messages' => $validator->errors()->all()];
        }

        return $this->changeTime($request, 'studio', 'add');
    }

    public function removeTimeForStudio(Request $request): array
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date|date_format:Y-m-d',
            'time' => 'required',
            'studioId' => 'required|exists:studios,id'
        ]);
        if ($validator->fails()) {
            return ['error' => true, 'messages' => $validator->errors()->all()];
        }

        return $this->changeTime($request, 'studio', 'delete');
    }

    private function changeTime(Request $request, $owner, $action)
    {
        $mDate = Jalalian::fromFormat('Y-m-d', $request->input('date'))->toCarbon();

        if ($mDate->greaterThan(Carbon::now()->addMonth())) {
            return ['error' => true, 'messages' => ['تاریخ انتخابی از یک ماه بیشتر است']];
        }

        if ($mDate->lessThan(Carbon::now()->format('Y-m-d'))) {
            return ['error' => true, 'messages' => ['تاریخ انتخابی برای گذشته است']];
        }

        $times = explode('_', $request->input('time'));
        foreach ($times as $time) {
            if (!AllowedHour::query()->where('id', $time)->exists()) {
                return ['error' => true, 'messages' => ['ورودی های ساعت اشتباه است. مجددا تلاش کنید']];
            }
        }

        if ($mDate->toDateString() == Carbon::now()->format('Y-m-d')) {
            foreach ($times as $time) {
                $allowedTime = AllowedHour::query()->where('id', $time)->first();
                $now = Carbon::now()->format('H:i:s');
                if (strtotime($now) > strtotime($allowedTime->started_at)) {
                    return ['error' => true, 'messages' => ['برای انتخاب یا تغییر بازه قبل از اکنون اقدام کنید']];
                }
            }
        }

        if ($owner === 'advisor')
            return $this->changeTimeForAdvisor($request, $action, $times);
        else
            return $this->changeTimeForStudio($request, $action, $times);
    }

    private function changeTimeForAdvisor(Request $request, $action, $times): array
    {
        $user = $this->user();
        $artist = Artist::where('user_id', $user->id)->first('id');
        if (is_null($artist)) {
            return ['error' => true, 'messages' => [__('site.no_artist')]];
        }

        if (!$artist->is_advisor) {
            $artist->is_advisor = true;
            $artist->save();
        }

        return $this->changeDetailsOfReservation($request, 'Artist', $artist, $action, $times);
    }

    private function changeTimeForStudio(Request $request, $action, $times): array
    {
        $result = $this->checkingStudioForUserAndGet($request->input('studioId'));
        if ($result['error'])
            return $result;

        $studio = $result['studio'];

        if ($studio->status == 'FAILED')
            return ['error' => true, 'messages' => ['مشکلی در اطلاعات استدیو شما وجود دارد.لطفا با پشتیبانی تماس بگیرید']];

        if (!$studio->is_active)
            return ['error' => true, 'messages' => ['شما وضعیت استدیو خود را غیر فعال گذاشتید، لطفا به فعال تغییر دهید']];

        return $this->changeDetailsOfReservation($request, 'Studio', $studio, $action, $times);
    }

    private function changeDetailsOfReservation(Request $request, $model, $item, $action, $times): array
    {
        $type = 'App\Models\\' . $model;
        $mDate = Jalalian::fromFormat('Y-m-d', $request->input('date'))->toCarbon();
        $resDate = $this->findReservationDate($mDate, $type, $item->id);
        if (is_null($resDate)) {
            if ($action == 'add') {
                $resDate = $this->createReservationDate($mDate, $type, $item->id);
            } else { //delete
                return ['error' => true, 'messages' => ['تاریخ انتخابی اشتباه است']];
            }
        }

        if ($action == 'add') {
            $result = $this->addDetailsOfReservation($times, $resDate);
            if ($result['error']) {
                return $result;
            }
        } else { //delete
            $result = $this->deleteDetailsOfReservation($times, $resDate);
            if ($result['error']) {
                return $result;
            }
        }

        return ['error' => false, 'dates' => $this->findReservationDetail([$resDate])[0]];
    }

    private function addDetailsOfReservation($times, $resDate): array
    {
        foreach ($times as $time) {
            if ($this->existReservationDetail($resDate->id, $time)) {
                return ['error' => true, 'messages' => ['بازه(ها) ی انتخابی ثبت شده است']];
            }
        }
        foreach ($times as $time) {
            $this->createReservationDetail($resDate->id, $time);
        }
        return ['error' => false];
    }

    private function deleteDetailsOfReservation($times, $resDate): array
    {
        foreach ($times as $time) {
            if (!$this->isReserveBySomeOne($resDate->id, $time)) {
                return ['error' => true, 'messages' => ['یکی از بازه های زمانی ثبت نشده است یا از قبل رزرو شده است']];
            }
        }

        foreach ($times as $time) {
            $this->deleteReservationDetail($resDate->id, $time);
        }

        return ['error' => false];
    }

    private function createReservationDate($mDate, $type, $id)
    {
        return ReservationDate::query()->create([
            'date' => $mDate,
            'owner_type' => $type,
            'owner_id' => $id,
        ]);
    }

    private function findReservationDate($mDate, $type, $id)
    {
        return ReservationDate::query()
            ->where('date', $mDate)
            ->where('owner_type', $type)
            ->where('owner_id', $id)
            ->first();
    }

    private function createReservationDetail($resDateId, $allowedHrId): void
    {
        ReservationDetail::query()->create([
            'res_date_id' => $resDateId,
            'allowed_hr_id' => $allowedHrId,
        ]);
    }

    private function deleteReservationDetail($resDateId, $allowedHrId): void
    {
        ReservationDetail::query()
            ->where('res_date_id', $resDateId)
            ->where('allowed_hr_id', $allowedHrId)
            ->delete();
    }

    private function existReservationDetail($resDateId, $allowedHrId): bool
    {
        return ReservationDetail::query()
            ->where('res_date_id', $resDateId)
            ->where('allowed_hr_id', $allowedHrId)
            ->exists();
    }

    private function isReserveBySomeOne($resDateId, $allowedHrId): bool
    {
        return ReservationDetail::query()
            ->where('res_date_id', $resDateId)
            ->where('allowed_hr_id', $allowedHrId)
            ->where('user_id', null)
            ->exists();
    }

    private function findReservationDetail($resDates)
    {
        foreach ($resDates as $resDate) {
            $resDetails = ReservationDetail::query()
                ->where('res_date_id', $resDate->id)
                ->get();

            if ($resDetails->count() == 0) {
                $resDate->delete();
                continue;
            }
            $resDetails = $this->wrappingReservationDetails($resDetails);

            $jDate = Jalalian::fromCarbon(
                ReservationDate::find($resDate->id)->date
//                Carbon::createFromFormat('Y-m-d H:i:s', ReservationDate::find($resDate->id)->date)
            );
            $resDate->shamsi_date_1 = $jDate->format('%A, %d %B');
            $resDate->shamsi_date_2 = $jDate->format('%Y-%m-%d');
            $resDate->details = $resDetails;
        }
        return $resDates;
    }

    private function wrappingReservationDetails($resDetails): array
    {
        $date = [];
        foreach ($resDetails as $resDetail) {
            $date[] = [
                'id' => $resDetail->id,
                'score' => $resDetail->score,
                'allowed_hour' => $this->wrappingAllowedHours($resDetail->allowedHour),
                'is_reserve' => !is_null($resDetail->user_id),
            ];
        }
        return $date;
    }

    private function wrappingAllowedHours($time): array
    {
        return [
            'id' => $time->id,
            'started_at' => date('h:i', strtotime($time->started_at)),
            'ended_at' => date('h:i', strtotime($time->ended_at)),
        ];
    }

    private function checkingStudioForUserAndGet($studioId): array
    {
        $user = $this->user();
        $studio = Studio::query()
            ->where('id', $studioId)
            ->where('user_id', $user->id)->first();
        if (is_null($studio))
            return ['error' => true, 'messages' => [__('site.select_wrong_studio')]];
        else
            return ['error' => false, 'studio' => $studio];
    }

    public function recordScore(Request $request): array
    {
        $validator = Validator::make($request->all(), [
            'owner' => [
                'required', Rule::in(['advisor', 'studio']),
            ],
            'date' => 'required|date|date_format:Y-m-d',
            'time' => 'required|exists:allowed_hours,id',
            'itemId' => 'required|numeric',
            'score' => 'required|integer|min:0|max:5'
        ]);
        if ($validator->fails()) {
            return ['error' => true, 'messages' => $validator->errors()->all()];
        }
        if ($request->input('owner') == 'advisor')
            $validator = Validator::make($request->all(), [
                'itemId' => 'exists:artists,id'
            ]);
        else
            $validator = Validator::make($request->all(), [
                'itemId' => 'exists:studios,id'
            ]);
        if ($validator->fails()) {
            return ['error' => true, 'messages' => $validator->errors()->all()];
        }

        $type = ($request->input('owner') == 'advisor')
            ? 'App\Models\Artist'
            : 'App\Models\Studio';

        $mDate = Jalalian::fromFormat('Y-m-d', $request->input('date'))->toCarbon();
        $resDate = $this->findReservationDate($mDate, $type, $request->input('itemId'));
        if (is_null($resDate))
            return ['error' => false, 'messages' => ['تاریخ رزروی برای مورد انتخابی شما وجود ندارد']];

        $resDetail = ReservationDetail::query()
            ->where('res_date_id', $resDate->id)
            ->where('allowed_hr_id', $request->input('time'))
            ->first();
        if (is_null($resDetail))
            return ['error' => false, 'messages' => ['زمان رزروی در تاریخ انتخابی وجود ندارد']];

        $user = $this->user();
        if ($resDetail->user_id != $user->id)
            return ['error' => false, 'messages' => ['شما نمی توانید امتیاز دهید']];

        $resDetail->score = $request->input('score');
        $resDetail->save();

        return ['error' => false, 'messages' => ['از همراهی شما سپاس گزاریم']];
    }

    public function getStatusOfReservationForUser(Request $request): array
    {
        $user = $this->user();

        $resDetails = $user->reservationDetails->map(function (ReservationDetail $reservationDetail) {
            $resDate = $reservationDetail->reservationDate;
            $jDate = Jalalian::fromCarbon($resDate->date);
            $reservationDetail->shamsi_date_1 = $jDate->format('%A, %d %B %Y');
            $reservationDetail->shamsi_date_2 = $jDate->format('%Y-%m-%d');
            $artistController = new ArtistController();
            $reservationDetail->artist = $artistController->assignArtists([$resDate->owner])[0];
            $reservationDetail->allowedHour;
            return $reservationDetail;
        });;

        $resDetails = $this->pagination($request, $resDetails, 7);

        return ['error' => false, 'details' => $resDetails];
    }
}
