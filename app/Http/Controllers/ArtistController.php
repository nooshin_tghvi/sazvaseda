<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArtistController extends Controller
{
    public function get(Request $request)
    {
        $artists = Artist::query()
            ->where('status', 'APPROVAL')
            ->get()
            ->map(function (Artist $artist) {
                $user = $artist->user;
                $artist->first_name = $user->first_name;
                $artist->last_name = $user->last_name;
                $artist->link = (new CreateLinkController)->__invoke('artist', $artist->id);
                $titleController = new TitleController();
                $artist->titles = $titleController->assignTitles($artist->titles);
                $artist->makeVisible(['titles']);
                return $artist;
            });
        $this->putImageURLInObjects($artists, ['avatar']);

        $rpp = $request->filled('rpp')
            ? $request->input('rpp')
            : 10;

        if ($request->filled('column') && $request->filled('isDesc'))
            $artists = $artists->sortBy($request->input('column'), SORT_REGULAR, $request->input('isDesc'));

        if ($request->filled('is_advisor'))
            $artists = $artists->where('is_advisor', $request->input('is_advisor'));

        if ($request->filled('ap_min_max') && sizeof(explode('_', $request->input('ap_min_max'))) == 2) {
            $min_max = explode('_', $request->input('ap_min_max'));
            $artists = $artists->whereBetween('advise_price', [$min_max[0], $min_max[1]]);
        }

        if ($request->filled('dt_min_max') && sizeof(explode('_', $request->input('dt_min_max'))) == 2) {
            $min_max = explode('_', $request->input('dt_min_max'));
            $artists = $artists->whereBetween('delivery_time', [$min_max[0], $min_max[1]]);
        }

        if ($request->filled('search')) {
            $name = $request->input('search');
            $artists = $artists->filter(function ($item) use ($name) {
                return strpos($item->first_name . ' ' . $item->last_name, $name) !== false;
            });
        }

        if ($request->filled('title')) {
            $title = $request->input('title');
            $artists = $artists->filter(function ($item) use ($title) {
                return count(array_filter($item->titles, function ($obj) use ($title) {
                    return $obj['name'] == $title;
                }));
            });
        }

        $artists = $this->pagination($request, $artists, $rpp);

        return response(['error' => false, 'artists' => $artists,
            'ap_min' => Artist::where('status', 'APPROVAL')->min('advise_price'),
            'ap_max' => Artist::where('status', 'APPROVAL')->max('advise_price'),
            'dt_min' => Artist::where('status', 'APPROVAL')->min('delivery_time'),
            'dt_max' => Artist::where('status', 'APPROVAL')->max('delivery_time'),
        ]);
    }

    public function detail($artistId)
    {
        if (!$this->isAllowed($artistId))
            return response(null, 404);

        $artist = $this->findArtistById($artistId);

        $user = $artist->user;
        $artist->first_name = $user->first_name;
        $artist->last_name = $user->last_name;
        $data = $this->prepareArtistInformation($artist);


        return response(['error' => false, 'data' => $data]);
    }

    public function assignArtists($artists): array
    {
        $data = [];
        foreach ($artists as $artist) {
            $user = $artist->user;
            array_push($data, [
                'id' => $artist->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'avatar' => $this->createURLForImage($artist->avatar),
                'link' => (new CreateLinkController)->__invoke('artist', $artist->id),
            ]);
        }
        return $data;
    }

//    public function search($item){
//        $artist = Artist::where()
//        $artist = Artist::where('id', $id)
//            ->first();
//        if (is_null($artist))
//            return false;
//        if ($artist->status != 'APPROVAL')
//            return false;
//        $user = $artist->user;
//        return $user->is_approved;
//    }

    public function register()
    {
        $user = $this->user();

        if (Artist::query()->where('user_id', $user->id)->exists())
            return response(['error' => true, 'messages' => ['شما به عنوان هنرمند ثبت نام کرده اید']]);

        $artist = Artist::create(['user_id' => $user->id]);

        $this->sendNotice('NEW_ARTIST) id:' . $artist->id);

        return response(['error' => false, 'artist' => $artist]);
    }

    public function me()
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $artist = $this->artist()->makeVisible('status');
        $data = $this->prepareArtistInformation($artist);

        return response(['error' => false, 'data' => $data]);
    }

    public function prepareArtistInformation($artist): array
    {
        $data['artist'] = $this->putImageURLInObjects([$artist], ['avatar'])[0];

        $artistPortfolioController = new ArtistPortfolioController();
        $data['portfolio'] = $artistPortfolioController->get($artist);

        $titleController = new TitleController ();
        $data['titles'] = $titleController->assignTitles($artist->titles);

        $packageController = new PackageController();
        $data['packages'] = $packageController->assignPackages($artist->packages);

        return $data;
    }

    public function completeAvatar(Request $request)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $validator = Validator::make($request->all(), [
            'avatar' => 'image|max:512', //512 KB
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $artist = $this->artist();
        $path = 'image/avatar/';
        if (!is_null($artist->avatar))
            $this->deleteFile($artist->avatar);

        if ($request->hasFile('avatar')) {
            $artist->avatar = $this->saveAndGetPathFile($path, $request->file('avatar'));
        } else {
            $artist->avatar = null;
        }
        $artist->save();

        return response(['error' => false, 'avatar' => $this->createURLForImage($artist->avatar)]);
    }

    public function completeExperience(Request $request)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $validator = Validator::make($request->all(), [
            'experience' => 'required|string|max:21500', //21500 char
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $artist = $this->artist();
        if ($artist->experience != $request->input('experience')) {
            $artist->experience = $request->input('experience');
            $artist->save();
        }
        return response(['error' => false, 'experience' => $artist->experience]);
    }

    public function completeOrderDescription(Request $request)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $validator = Validator::make($request->all(), [
            'order_description' => 'required|string|max:21500', //21500 char
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $artist = $this->artist();
        if ($artist->order_description != $request->input('order_description')) {
            $artist->order_description = $request->input('order_description');
            $artist->save();
        }
        return response(['error' => false, 'order_description' => $artist->order_description]);
    }

    public function completeDeliveryTime(Request $request)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $validator = Validator::make($request->all(), [
            'delivery_time' => 'required|integer|max:90|min:1',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $artist = $this->artist();
        if ($artist->delivery_time != $request->input('delivery_time')) {
            $artist->delivery_time = $request->input('delivery_time');
            $artist->save();
        }
        return response(['error' => false, 'delivery_time' => $artist->delivery_time]);
    }

    public function changeAcceptAdvisor()
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $artist = $this->artist();
        $artist->is_advisor = !$artist->is_advisor;
        $artist->save();

        return response(['error' => false, 'being_advisor' => $artist->is_advisor]);
    }

    public function completeAdvisePrise(Request $request)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $validator = Validator::make($request->all(), [
            'advise_price' => 'required|integer|min:0',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $artist = $this->artist();
        if (!$artist->is_advisor)
            return response(['error' => true, 'messages' => ['ابتدا گزینه پذیرش مشاوره را قبول کنید']]);

        if ($artist->advise_price != $request->input('advise_price')) {
            $artist->advise_price = $request->input('advise_price');
            $artist->save();
        }
        return response(['error' => false, 'advise_price' => $artist->advise_price]);
    }

    public function isAllowed($id): bool
    {
        return Artist::query()
            ->where('id', $id)
            ->where('status', 'APPROVAL')
            ->exists();
    }

    public function findArtistById($id)
    {
        return Artist::find($id);
    }

}
