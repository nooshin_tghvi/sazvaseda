<?php

namespace App\Http\Controllers;

use App\Models\FileBank;
use Illuminate\Http\Request;

class ArtistPortfolioController extends PortfolioController
{
    public function __construct()
    {
        $this->ownerType = 'App\Models\Artist';
        $this->pathImage = 'file_bank/artist/image/';
        $this->pathSound = 'file_bank/artist/sound/';
    }

    public function get($artist)
    {
        $portfolios = $artist->contents;
        $this->putImageURLInObjects($portfolios, ['sound', 'image']);
        return $portfolios;
    }

    public function detail($id)
    {
        $result = $this->isPortfolioForArtist($id);
        if ($result['error'])
            return response($result);

        $fb = parent::detail($id);
        $portfolio = $this->putImageURLInObjects([$fb], ['sound', 'image']);
        return response(['error' => false, 'portfolio' => $portfolio]);
    }

    public function register(Request $request)
    {
        if (!$this->existsArtist())
            return response(['error' => true, 'messages' => [__('site.no_artist')]]);

        $artist = $this->artist();
        $this->ownerId = $artist->id;
        return parent::register($request);
    }

    public function delete($id)
    {
        $result = $this->isPortfolioForArtist($id);
        if ($result['error'])
            return response($result);

        return parent::delete($id);
    }

    private function isPortfolioForArtist($id): array
    {
        if (!$this->existsArtist())
            return ['error' => true, 'messages' => [__('site.no_artist')]];
        $artist = $this->artist();
        if (!FileBank::where('id', $id)
            ->where('owner_type', $this->ownerType)
            ->where('owner_id', $artist->id)->exists())
            return ['error' => true, 'messages' => 'در انتخاب نمونه ی کار دقت کنید'];
        return ['error' => false];
    }

    protected function edit(Request $request, $id)
    {
        $result = $this->isPortfolioForArtist($id);
        if ($result['error'])
            return response($result);

        return parent::edit($request, $id);
    }

    protected function changeName(Request $request, $id)
    {
        $result = $this->isPortfolioForArtist($id);
        if ($result['error'])
            return response($result);

        return parent::changeName($request, $id);
    }

    protected function changeDate(Request $request, $id)
    {
        $result = $this->isPortfolioForArtist($id);
        if ($result['error'])
            return response($result);

        return parent::changeDate($request, $id);
    }

    protected function changeSaleability($id)
    {
        $result = $this->isPortfolioForArtist($id);
        if ($result['error'])
            return response($result);

        return parent::changeSaleability($id);
    }

    protected function changeDescription(Request $request, $id)
    {
        $result = $this->isPortfolioForArtist($id);
        if ($result['error'])
            return response($result);

        return parent::changeDescription($request, $id);
    }

    protected function changeFiles(Request $request, $id)
    {
        $result = $this->isPortfolioForArtist($id);
        if ($result['error'])
            return response($result);

        return parent::changeFiles($request, $id);
    }

}
