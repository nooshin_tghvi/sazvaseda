<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Cart;
use App\Models\ItemCart;
use App\Models\Package;
use App\Models\ReservationDate;
use App\Models\ReservationDetail;
use App\Models\Studio;
use Illuminate\Http\Request;
use JsonException;
use Morilog\Jalali\Jalalian;

class CartController extends Controller
{
    /**
     * @throws JsonException
     */
    public function get()
    {
        $result = $this->update();
        return response([
            'error' => false,
            'cart' => $this->jsonDecodeDetailsOfCart($result['cart']),
            'messages' => $result['messages']
        ]);
    }

    public function create($userId): void
    {
        Cart::create(['user_id' => $userId, 'details' => '[]']);
    }

    /**
     * @throws JsonException
     */
    private function jsonDecodeDetailsOfCart($cart)
    {
        $cart->details = json_decode($cart->details, false, 512, JSON_THROW_ON_ERROR);
        return $cart;
    }

    /**
     * @throws JsonException
     */
    private function update(): array
    {
        $user = $this->user();
        $cart = $user->LastUnpaidCart;
        $details = json_decode($cart->details, true, 512, JSON_THROW_ON_ERROR);
        $items = $cart->items($cart->id);

        $products = [];
        $messages = [];
        $finalCost = 0;
        foreach ($items as $item) {
            $previousPrice = $this->findPreviousPrice($details, $item->item_id, $item->type);
            $result = $this->preparingDataForCart($cart->id, strtolower($item->type), $item->item_id, $previousPrice);
            if ($result['error']) {
                $messages[] = $result['message'];
            } else {
                $products[] = [
                    'id' => $item->item_id,
                    'image' => $this->createURLForImage($result['image']),
                    'full_name' => $result['fullName'],
                    'type_id' => $result['id'],
                    'price' => $result['price'],
                    'type' => strtolower($item->type),
                    'details' => $result['details'],
                ];
                $finalCost += $result['price'];
                if (!is_null($result['message'])) {
                    $messages[] = $result['message'];
                }
            }
        }

        $cart->details = json_encode($products, JSON_THROW_ON_ERROR);
        $cart->final_cost = $finalCost;
        $cart->save();

        return ['messages' => $messages, 'cart' => $cart];
    }

    private function findPreviousPrice($details, $itemId, $type)
    {
        foreach ($details as $item) {
            if ($item['id'] == $itemId && $item['type'] == strtolower($type)) {
                return $item['price'];
            }
        }
    }

    private function preparingDataForCart($cartId, $type, $itemId, $previousPrice): array
    {
        if ($type === 'studio') {
            $result = $this->checkingReservation($itemId, 'Studio');
            if ($result['error']) {
                return $result;
            }

            $studioController = new StudioController();
            $studio = $studioController->findAllowedStudioToReserve($result['resDate']->owner_id);
            if (!is_null($studio)) {
                $msg = $this->checkingChangingPrice('studio', $studio->price, $previousPrice, $studio->name);
                return ['error' => false, 'image' => $studio->logo, 'fullName' => $studio->name, 'id' => $studio->id,
                    'price' => $studio->price, 'message' => $msg, 'details' => $result['details']];
            }

            $studio = Studio::find($itemId);
            $this->deleteItemCartFromDB($cartId, $itemId, $type);
            $msg = __('site.can_not_reserve_studio', ['name' => $studio->name]);
            return ['error' => true, 'message' => $msg];
        }

        if ($type === 'package') {
            $packageController = new PackageController();
            $package = $packageController->findAllowedPackage($itemId);
            if (!is_null($package)) {
                $msg = $this->checkingChangingPrice('package', $package->price, $previousPrice, $package->name);
                return ['error' => false, 'image' => $package->image, 'fullName' => $package->name, 'id' => $package->id,
                    'price' => $package->price, 'message' => $msg, 'details' => []];
            }

            $package = Package::find($itemId);
            $this->deleteItemCartFromDB($cartId, $itemId, $type);
            $msg = __('site.can_not_buy_package', ['name' => $package->name]);
            return ['error' => true, 'message' => $msg];
        }

        if ($type === 'advisor') {
            $result = $this->checkingReservation($itemId, 'Artist');
            if ($result['error']) {
                return $result;
            }

            $artistController = new ArtistController();
            if ($artistController->isAllowed($result['resDate']->owner_id)) {
                $artist = $artistController->findArtistById($result['resDate']->owner_id);
                $user = $artist->user;
                $fullName = $user->fullName();
                if ($artist->is_advisor) {
                    $msg = $this->checkingChangingPrice('advisor', $artist->advise_price, $previousPrice, $fullName);
                    return ['error' => false, 'image' => $artist->avatar, 'fullName' => $fullName, 'id' => $artist->id,
                        'price' => $artist->advise_price, 'message' => $msg, 'details' => $result['details']];
                }

                $msg = __('site.can_not_reserve_advisor', ['name' => $fullName]);
                return ['error' => true, 'message' => $msg];
            }

            $this->deleteItemCartFromDB($cartId, $itemId, $type);
            return ['error' => true, 'message' => 'هنرمند مورد نظر یافت نشد'];
        }

        if ($type === 'teammate') {
            $titleController = new TitleController();
            if ($titleController->existTitleArtist($itemId)) {
                $titleArtist = $titleController->getTitleArtist($itemId);
                $artistController = new ArtistController();
                if ($artistController->isAllowed($titleArtist->artist_id)) {
                    $artist = $artistController->findArtistById($titleArtist->artist_id);
                    $user = $artist->user;
                    $fullName = $user->fullName();
                    if ($titleArtist->accept_order) {
                        $msg = $this->checkingChangingPrice('teammate', $titleArtist->order_price, $previousPrice, $fullName);
                        return ['error' => false, 'image' => $artist->avatar, 'fullName' => $fullName, 'id' => $artist->id,
                            'price' => $titleArtist->order_price, 'message' => $msg, 'details' => []];
                    }

                    $this->deleteItemCartFromDB($cartId, $itemId, $type);
                    $msg = __('site.not_accept_to_be_a_teammate', ['name' => $fullName]);
                    return ['error' => true, 'message' => $msg];
                }

                $this->deleteItemCartFromDB($cartId, $itemId, $type);
                $artist = Artist::find($itemId);
                $user = $artist->user;
                $fullName = $user->fullName();
                $msg = __('site.deactivate_artist', ['name' => $fullName]);
                return ['error' => true, 'message' => $msg];
            }

            $this->deleteItemCartFromDB($cartId, $itemId, $type);
            return ['error' => true, 'message' => 'شناسه مورد نظر یافت نشد'];
        }

        return ['error' => true, 'message' => 'تایپ وارد شده اشتباه است'];
    }

    /**
     * @throws JsonException
     */
    public function addItemToCart($type, Request $request)
    {
        if (!$request->filled('itemId')) {
            return response(['error' => true, 'messages' => 'شناسه ی محصول وجود ندارد']);
        }

        $itemId = $request->input('itemId');
        $result = $this->checkingRequest($type, $itemId);
        if ($result['error']) {
            return response($result);
        }

        $user = $this->user();
        $cart = $user->LastUnpaidCart;
        $details = json_decode($cart->details, true, 512, JSON_THROW_ON_ERROR);
        foreach ($details as $item) {
            if ($item['id'] == $itemId && $item['type'] == $type) {
                return response(['error' => true, 'messages' => ['مورد انتخابی در سبد خرید موجود است']]);
            }
        }

        ItemCart::create([
            'cart_id' => $cart->id,
            'type' => strtoupper($type),
            'item_id' => $itemId
        ]);

        $details[] = [
            'id' => $itemId,
            'image' => $this->createURLForImage($result['image']),
            'full_name' => $result['fullName'],
            'type_id' => $result['id'],
            'price' => $result['price'],
            'type' => $type,
            'details' => $result['details']
        ];

        $cart->details = json_encode($details, JSON_THROW_ON_ERROR);
        $cart->final_cost += $result['price'];
        $cart->save();

        return response([
            'error' => false,
            'cart' => $this->jsonDecodeDetailsOfCart($cart),
            'messages' => ['مورد انتخابی به سبد خرید اضافه شد']
        ]);
    }

    /**
     * @throws JsonException
     */
    public function deleteItemFromCart($type, Request $request)
    {
        if (!$request->filled('itemId')) {
            return response(['error' => true, 'messages' => ['شناسه ی محصول وجود ندارد']]);
        }

        $itemId = $request->input('itemId');
        $user = $this->user();
        $cart = $user->LastUnpaidCart;
        $itemsInfo = json_decode($cart->details, true, 512, JSON_THROW_ON_ERROR);

        $existItemOnCart = false;
        foreach ($itemsInfo as $key => $item) {
            if ($item['id'] == $itemId && $item['type'] == $type) {
                $existItemOnCart = true;
                $cart->final_cost -= $item['price'];
                unset($itemsInfo[$key]);
                break;
            }
        }
        if (!$existItemOnCart) {
            return response(['error' => true, 'messages' => ['مورد انتخابی در سبد خرید وجود ندارد']]);
        }

        $cart->details = json_encode($itemsInfo, JSON_THROW_ON_ERROR);
        $cart->save();

        $this->deleteItemCartFromDB($cart->id, $itemId, $type);

        return response([
            'error' => false,
            'cart' => $this->jsonDecodeDetailsOfCart($cart), 'messages' => ['مورد انتخابی با موفقیت حذف شد']
        ]);
    }

    private function deleteItemCartFromDB($cartId, $itemId, $type): void
    {
        ItemCart::where('cart_id', $cartId)
            ->where('item_id', $itemId)
            ->where('type', strtoupper($type))
            ->delete();
    }

    private function checkingRequest($type, $itemId): array
    {
        if (!in_array($type, ['advisor', 'teammate', 'package', 'studio'])) {
            return ['error' => true, 'messages' => ['تایپ انتخابی اشتباه است']];
        }

        if ($type === 'studio') {
            $result = $this->checkingReservation($itemId, 'Studio');
            if ($result['error']) {
                return $result;
            }

            $studioController = new StudioController();
            $studio = $studioController->findAllowedStudioToReserve($result['resDate']->owner_id);
            if (!is_null($studio)) {
                return ['error' => false, 'image' => $studio->logo, 'fullName' => $studio->name,
                    'id' => $studio->id, 'price' => $studio->price, 'details' => $result['details']];
            }

            return ['error' => true, 'messages' => ['استدیو مورد نظر یافت نشد']];
        }

        if ($type === 'package') {
            $packageController = new PackageController();
            $package = $packageController->findAllowedPackage($itemId);
            if (!is_null($package)) {
                return ['error' => false, 'image' => $package->image, 'fullName' => $package->name,
                    'id' => $package->id, 'price' => $package->price, 'details' => []];
            }

            return ['error' => true, 'messages' => ['پکیج مورد نظر یافت نشد']];
        }

        if ($type === 'teammate') {
            $titleController = new TitleController();
            if ($titleController->existTitleArtist($itemId)) {
                $titleArtist = $titleController->getTitleArtist($itemId);
                $artistController = new ArtistController();
                if ($artistController->isAllowed($titleArtist->artist_id)) {
                    $artist = $artistController->findArtistById($titleArtist->artist_id);
                    $user = $artist->user;
                    if ($titleArtist->accept_order) {
                        return ['error' => false, 'image' => $artist->avatar, 'fullName' => $user->fullName(),
                            'id' => $artist->id, 'price' => $titleArtist->order_price, 'details' => []];
                    }

                    return ['error' => true, 'messages' => ['هنرمند سفارش نمی پذیرد']];
                }

                return ['error' => true, 'messages' => ['هنرمند مورد نظر یافت نشد']];
            }

            return ['error' => true, 'messages' => ['شناسه مورد نظر یافت نشد']];
        }

        if ($type === 'advisor') {
            $result = $this->checkingReservation($itemId, 'Artist');
            if ($result['error']) {
                return $result;
            }

            $artistController = new ArtistController();
            if ($artistController->isAllowed($result['resDate']->owner_id)) {
                $artist = $artistController->findArtistById($result['resDate']->owner_id);
                $user = $artist->user;
                if ($artist->is_advisor) {
                    return ['error' => false, 'image' => $artist->avatar, 'fullName' => $user->fullName(),
                        'id' => $artist->id, 'price' => $artist->advise_price, 'details' => $result['details']];
                }

                return ['error' => true, 'messages' => ['هنرمند مشاوره نمی دهد']];
            }

            return ['error' => true, 'messages' => ['هنرمند مورد نظر یافت نشد']];
        }
        return ['error' => true, 'messages' => ['لطفا مجددا تلاش کنید']];
    }

    private function checkingReservation($itemId, $type): array
    {
        $resDetail = ReservationDetail::query()->find($itemId);
        if (is_null($resDetail)) {
            return ['error' => true, 'messages' => ['ورودی اشتباه است']];
        }
        if (!is_null($resDetail->user_id)) {
            return ['error' => true, 'messages' => ['زمان انتخابی رزرو شده است']];
        }
        $resDate = ReservationDate::query()
            ->where('id', $resDetail->res_date_id)
            ->where('owner_type', 'App\Models\\' . $type)
            ->first();
        if (is_null($resDate)) {
            return ['error' => true, 'messages' => ['ورودی اشتباه است']];
        }

        $jDate = Jalalian::fromCarbon($resDate->date);

        $details = [
            'shamsi_date_1' => $jDate->format('%A, %d %B'),
            'shamsi_date_2' => $jDate->format('%Y-%m-%d'),
            'time' => $resDetail->allowedHour,
        ];
        return ['error' => false, 'resDate' => $resDate, 'details' => $details];
    }

    private function checkingChangingPrice($type, $price, $previousPrice, $fullName)
    {
        if ($price == $previousPrice) {
            return null;
        }

        $diff = $price - $previousPrice;
        $sign = ($diff > 0) ? 'بیشتر' : 'کمتر';
        return __('site.change_cost_of_' . $type, [
            'name' => $fullName,
            'number' => abs($diff),
            'sign' => $sign
        ]);
    }
}
