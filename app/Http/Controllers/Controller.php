<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     *
     * @return User|Authenticatable
     */
    protected function user(): User
    {
        return Auth::user();
    }

    protected function existsArtist(): bool
    {
        $user = $this->user();
        $artist = Artist::where('user_id', $user->id)->first('id');
        return !is_null($artist);
    }

    protected function artist(): Artist
    {
        return Auth::user()->artist;
    }

    protected function getName($n): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }
        return $randomString;
    }

    protected function getNumber($n): string
    {
        $characters = '012345678901234567890123456789';
        $randomString = '';
        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }
        return $randomString;
    }

    //$request->file('file')->extension()
    //$request->file('file')
    protected function saveAndGetPathFile($path, $file): string
    {
        $newName = $this->getName(15) . '.' . $file->extension();
        while (Storage::exists('public/' . $path . $newName)) {
            $newName = $this->getName(15) . '.' . $file->extension();
        }
        Storage::putFileAs('public/' . $path, $file, $newName);
        return $path . $newName;
    }

    protected function deleteFile($name)
    {
        Storage::delete('public/' . $name);
    }

    protected function createURLForImage($image): ?string
    {
        return is_null($image)
            ? null
            : config('app.url') . Storage::url($image);
    }

    protected function putImageURLInObjects($objects, $imageSubjectArr)
    {
        foreach ($objects as $object)
            foreach ($imageSubjectArr as $image) {
                if (!is_null($object[$image])) {
                    $object[$image] = $this->createURLForImage($object[$image]);
                }
            }
        return $objects;
    }

    protected function logMsg($content)
    {
        log::info($content);
    }

    protected function sendNotice($content)
    {
//        Notification::route('slack', config('slack.general_channel'))
//            ->notify(new NewEvent('#general', $content));
    }

    protected function pagination(Request $request, $items, $rpp): LengthAwarePaginator
    {
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(
            array_values($items->forPage($request->filled('page') ? $request->input('page') : 1, $rpp)->toArray()),
            $items->count(),
            $rpp,
            $request->filled('page') ? $request->input('page') : 1,
            ['path' => $request->url()]
        );
    }

}
