<?php

namespace App\Http\Controllers;

use App\Models\Discount;
use App\Models\ReservationDetail;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Morilog\Jalali\Jalalian;

class TransactionController extends Controller
{
    private PaypingController $paypingController;
    private CartController $cartController;

    public function __construct()
    {
        $this->paypingController = new PaypingController();
        $this->cartController = new CartController();
    }

    private function allowToCalculatePayment(Request $request): array
    {
        $validator = Validator::make($request->all(), [
            'code' => 'nullable|exists:discounts',
            'description' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return ['error' => true, 'messages' => $validator->errors()->all()];
        }

        if ($this->isThereDiscountCode($request)) {
            return $this->applyDiscountOnPayment($request);
        } else {
            $user = $this->user();
            $cart = $user->LastUnpaidCart;
            return [
                'error' => false,
                'amountOfDiscount' => 0,
                'amountOfPayment' => $cart->final_cost,
                'cart' => $cart,
                'discount' => null
            ];
        }
    }

    private function isThereDiscountCode(Request $request): bool
    {
        return $request->filled('code');
    }

    private function applyDiscountOnPayment(Request $request): array
    {
        $discount = Discount::where('code', $request->input('code'))->first();
        $discountController = new DiscountController;
        $result = $discountController->checkingValidation($discount);
        if ($result['error'])
            return $result;

        $content = $discountController->applyDiscountCode($discount, $result['cart']);
        return [
            'error' => false,
            'amountOfDiscount' => $content['amountOfDiscount'],
            'amountOfPayment' => $content['amountOfPayment'],
            'cart' => $result['cart'],
            'discount' => $discount
        ];
    }

    public function buy(Request $request)
    {
        $result = $this->allowToCalculatePayment($request);
        if ($result['error']) {
            return response($result);
        }

        $amountOfPayment = $result['amountOfPayment'];
        $cart = $result['cart'];
        $discount = $result['discount'];

        $factorNumber = $this->createFactorNumber();

        Transaction::create([
            'amount' => $amountOfPayment,
            'factor_number' => $factorNumber,
            'cart_id' => $cart->id,
            'discount_id' => is_null($discount) ? null : $discount->id,
        ]);

        $user = $this->user();
        $this->paypingController->createBodyForPay(
            $user->fullName(),
            $cart->final_cost,
            $user->mobile,
            $factorNumber
        );
        $result = $this->paypingController->pay();
        if ($result['error'])
            return response($result);

        $this->paypingController->setPayUrl($result['code']);
        return response(['error' => false, 'redirect_url' => $this->paypingController->getPayUrl()]);
    }

    public function verify(Request $request, $factorNumber)
    {
        $tran = Transaction::where('factor_number', $factorNumber)->firstOrFail();
        $this->paypingController->createBodyForVerify($request->input('refId'), $request->input('amount'));

        $result = $this->paypingController->verify();
        if ($result['error']) {
            $tran->condition = 'FAILED';
            $tran->save();
            return response($result);
        }

        $tran->condition = 'SUCCESSFUL';
        $tran->card_number = $result['cardNumber'];
        $tran->date_paid = Jalalian::now();
        $tran->save();

        $cart = $tran->cart;
        $cart->is_pay = true;
        $cart->transaction_id = $tran->id;
        $cart->save();

        $this->registerReservation($cart);

        $this->cartController->create($cart->user_id);

        if ($tran->discount_id != null) {
            $discount = $tran->discount;
            $discount->used_number = $discount->used_number + 1;
            $discount->save();
        }

        $this->sendNotice('NEW_ORDER) ' . $tran->factor_number);

        return response(['error' => false]);
    }

    public function myTransaction()
    {
        $user = $this->user();
        $carts = $user->carts;
        $orderRecords = [];
        foreach ($carts as $cart) {
            if ($cart->is_pay) {
                $transaction = Transaction::where('cart_id', $cart->id)->first();
                if ($transaction->condition == 'SUCCESSFUL') {
                    $cart_ = [];
                    $cart_['details'] = $cart->details;
                    $cart_['amount'] = $transaction->amount;
                    $cart_['factor_number'] = $transaction->factor_number;
                    $cart_['date_paid'] = $transaction->date_paid;
                    array_push($orderRecords, $cart_);
                }
            }
        }
        return response(['error' => false, 'carts' => $orderRecords]);
    }

    private function createFactorNumber(): string
    {
        $factorNumber = Str::random(10);
        while (Transaction::where('factor_number', $factorNumber)->exists()) {
            $factorNumber = Str::random(10);
        }
        return $factorNumber;
    }

    private function registerReservation($cart)
    {
        $details = json_decode($cart->details, true);
        foreach ($details as $item)
            if ($item['type'] == 'advisor' || $item['type'] == 'studio') {
                $resDetail = ReservationDetail::query()->find($item['id']);
                $user = $this->user();
                if (is_null($resDetail->user_id)) {
                    $resDetail->user_id = $user->id;
                    $resDetail->save();
                } else {
                    $this->sendNotice('Duplicate_Reserve) ' . $user->id);
                }
            }
    }
}
