<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Throwable;

class UserController extends Controller
{
    public function afterRegister($userId)
    {
        $cartController = new CartController();
        $cartController->create($userId);
    }

    public function afterLogin()
    {

    }

    public function completeMelliCode(Request $request)
    {
        $user = $this->user();
        $validator = Validator::make($request->all(), [
            'melli_code' => [
                'required', 'ir_national_code', 'size:10',
                Rule::unique('users')->ignore($user->id),
            ]
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }
        if ($user->melli_code != $request->input('melli_code')) {
            $user->melli_code = $request->input('melli_code');
            $user->save();
        }

        return response(['error' => false, 'melli_code' => $user->melli_code]);
    }

    public function completeIBAN(Request $request)
    {
        $user = $this->user();
        $validator = Validator::make($request->all(), [
            'iban' => [
                'required', 'ir_sheba', 'size:26',
                Rule::unique('users')->ignore($user->id),
            ],
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }
        if ($user->iban != $request->input('iban')) {
            $user->iban = $request->input('iban');
            $user->save();
        }
        return response(['error' => false, 'iban' => $user->iban]);
    }

    private function validationForSendCode($action, Request $request): array
    {
        if ($action == 'register') {
            $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|size:11|unique:users',
            ]);
        } elseif ($action == 'change') {
            try {
                $user = $this->user(); //todo check after login
            } catch (Throwable $exception) {
                return ['error' => true, 'messages' => ['برای تغییر شماره همراه ابتدا وارد سایت شوید']];
            }
            $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|size:11|unique:users',
            ]);
        } elseif ($action == 'forgot') {
            $validator = Validator::make($request->all(), [
                'mobile' => 'required|string|size:11|exists:users',
            ]);
        } else {
            return ['error' => true, 'messages' => ['وروردی اشتباه است. مجددا تلاش کنید']];
        }
        if ($validator->fails()) {
            return ['error' => true, 'messages' => $validator->errors()->all()];
        } else
            return ['error' => false];
    }

    public function sendSMSFor($action, Request $request)
    {
        $result = $this->validationForSendCode($action, $request);
        if ($result['error'])
            return response($result);

        $smsController = new SMSController();
        $result = $smsController->sendCodeToUser($request->input('mobile'));
        return response($result);
    }

    public function verifyMobileFor($action, Request $request)
    {
        $result = $this->validationForSendCode($action, $request);
        if ($result['error'])
            return response($result);

        $validator = Validator::make($request->all(), [
            'code' => 'required|string|size:6'
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $smsController = new SMSController();
        $result = $smsController->verifyCode($request->input('mobile'), $request->input('code'));
        return response($result);
    }

    public function changeMobile(Request $request)
    {
        $user = $this->user();
        $smsController = new SMSController();
        if (!$smsController->isVerifiedMobile($request->input('mobile'))) {
            return response(['error' => true, 'messages' => ['مدت زیادی از تایید کد گذشته است. لطفا مجددا تلاش کنید']]);
        }

        if ($user->mobile != $request->input('mobile')) {
            $user->mobile = $request->input('mobile');
            $user->save();
        }
        return response(['error' => false, 'mobile' => $user->mobile]);
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|string|size:11',
            'password' => 'required|string|min:8|confirmed',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $mobile = $request->input('mobile');
        $smsController = new SMSController();
        if (!$smsController->isVerifiedMobile($mobile)) {
            return response(['error' => true, 'messages' => ['مدت زیادی از تایید کد گذشته است. لطفا مجددا تلاش کنید']]);
        }

        $user = User::where('mobile', $mobile)->first();
        if (!Hash::check($request->input('password'), $user->password)) {
            $user->password = Hash::make($request->input('password'));
            $user->save();
        }
        return response(['error' => false, 'messages' => ['رمز عبور با موفقیت تغییر کرد']]);
    }

    public function hasStudio()
    {
        $user = $this->user();
        if (sizeof($user->studios) == 0)
            return response(['error' => false, 'has_studio' => false]);
        else
            return response(['error' => false, 'has_studio' => true]);
    }
}
