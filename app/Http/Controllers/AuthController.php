<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class AuthController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:user')->except('logout');
    }*/

    protected function guard()
    {
        return Auth::guard('user');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|persian_alpha|max:75',
            'last_name' => 'required|persian_alpha|max:75',
            'email' => 'required|email|max:150|unique:users',
            'mobile' => 'required|string|size:11|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $smsController = new SMSController();
        if (!$smsController->isVerifiedMobile($request->input('mobile'))) {
            return response(['error' => true, 'messages' => ['مدت زیادی از تایید کد گذشته است. لطفا مجددا تلاش کنید']]);
        }

        $request['password'] = Hash::make($request->input('password'));
        $request['remember_token'] = Str::random(10);
        $user = User::create($request->toArray());

        $accessToken = $user->createToken('authToken')->accessToken;

        $userController = new UserController();
        $userController->afterRegister($user->id);

        return response(['error' => false, 'access_token' => $accessToken]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|string|size:11|exists:users',
            'password' => 'required|string|min:8',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }
        /**
         * @var $user User
         */
        $user = User::where('mobile', $request->input('mobile'))->first();
        if (!Hash::check($request->input('password'), $user->password))
            return response(['error' => true, 'messages' => [__('auth.password')]]);

        //$user->tokens()->delete();
        $accessToken = $user->createToken('authToken')->accessToken;

        $userController = new UserController(); //todo check
        $userController->afterLogin();

        return response(['error' => false, 'access_token' => $accessToken]);
    }

    public function logout()
    {
        Auth::user()->token()->revoke();
        return response(null, 204);
    }

    public function me()
    {
        $user = $this->user();
        $data['user'] = $user->makeVisible('is_approved');

        $artistController = new ArtistController();
        if ($artistController->existsArtist()) {
            $data['is_artist'] = true;
            $data['other_info'] = $artistController->prepareArtistInformation($user->artist);
        } else
            $data['is_artist'] = false;

        return response(['error' => false, 'data' => $data]);
    }

    public function changeFirstName(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|persian_alpha|max:75',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $user = $this->user();
        if ($user->first_name != $request->input('first_name')) {
            $user->first_name = $request->input('first_name');
            $user->save();
        }
        return response(['error' => false, 'first_name' => $user->first_name]);
    }

    public function changeLastName(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'last_name' => 'required|persian_alpha|max:75',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $user = $this->user();
        if ($user->last_name != $request->input('last_name')) {
            $user->last_name = $request->input('last_name');
            $user->save();
        }
        return response(['error' => false, 'last_name' => $user->last_name]);
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|string|min:8',
            'password' => 'required|string|min:8|confirmed',
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $user = $this->user();
        if (!Hash::check($request->input('old_password'), $user->password)) {
            return response(['error' => true, 'messages' => ['رمز عبور اشتباه است']]);
        }

        $user->password = Hash::make($request->input('password'));
        $user->save();
        return response(['error' => false, 'messages' => ['رمز عبور با موفقیت تغییر کرد']]);
    }

    public function changeEmail(Request $request)
    {
        $user = $this->user();
        $validator = Validator::make($request->all(), [
            'email' => [
                'required', 'email', 'max:191',
                Rule::unique('users')->ignore($user->id),
            ],
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        if ($user->email != $request->input('email')) {
            $user->email = $request->input('email');
            $user->save();
        }
        return response(['error' => false, 'email' => $user->email]);
    }
}
