<?php

namespace App\Http\Controllers;

use App\Models\ArtistPackage;
use App\Models\Package;
use App\Models\Title;
use App\Models\TitlePackage;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function get(Request $request)
    {
        $packages = Package::query()
            ->where('is_viewable', true)
            ->get()
            ->map(function (Package $package) {
                $package->link = (new CreateLinkController)->__invoke('package', $package->id);
                $package->members = $this->addMembers($package);
                return $package;
            });
        $this->putImageURLInObjects($packages, ['image']);

        $rpp = $request->filled('rpp')
            ? $request->input('rpp')
            : 10;

        if ($request->filled('column') && $request->filled('isDesc'))
            $packages = $packages->sortBy($request->input('column'), SORT_REGULAR, $request->input('isDesc'));

        if ($request->filled('p_min_max') && sizeof(explode('_', $request->input('p_min_max'))) == 2) {
            $min_max = explode('_', $request->input('p_min_max'));
            $packages = $packages->whereBetween('price', [$min_max[0], $min_max[1]]);
        }

        if ($request->filled('dt_min_max') && sizeof(explode('_', $request->input('dt_min_max'))) == 2) {
            $min_max = explode('_', $request->input('dt_min_max'));
            $packages = $packages->whereBetween('delivery_time', [$min_max[0], $min_max[1]]);
        }

        if ($request->filled('po_min')) {
            $packages = $packages->where('po_number', '>=', $request->input('po_min'));
        }

        if ($request->filled('search')) {
            $name = $request->input('search');
            $packages = $packages->filter(function ($item) use ($name) {
                return strpos($item->name, $name) !== false;
            });
        }

        if ($request->filled('artistIds')) {
            $artistIds = explode('_', $request->input('artistIds'));
            $pa = [];
            foreach ($packages as $package) {
                foreach ($package['members'] as $member) {
                    if (in_array($member['id'], $artistIds)) {
                        array_push($pa, $package);
                        break;
                    }
                }
            }
            $packages = $pa;
        }

        if ($request->filled('titleIds')) {
            $titleIds = explode('_', $request->input('titleIds'));
            $pa = [];
            $memberBreak = false;
            foreach ($packages as $package) {
                foreach ($package['members'] as $member) {
                    foreach ($member['titles'] as $title) {
                        if (in_array($title['id'], $titleIds)) {
                            array_push($pa, $package);
                            $memberBreak = true;
                            break;
                        }
                    }
                    if ($memberBreak) {
                        $memberBreak = false;
                        break;
                    }
                }
            }
            $packages = $pa;
        }

        $packages = $this->pagination($request, $packages, $rpp);

        return response([
            'error' => false,
            'packages' => $packages,
            'p_max' => Package::where('is_viewable', true)->max('price'),
            'p_min' => Package::where('is_viewable', true)->min('price')
        ]);
    }

    public function detail($packageId)
    {
        $package = $this->findAllowedPackage($packageId);
        if (is_null($package))
            return response(['error' => true, 'messages' => ['استدیو مورد نظر پیدا نشد']]);

        $this->putImageURLInObjects([$package], ['image']);

        $data = $package;
        $data['members'] = $this->addMembers($package);
        $data['contents'] = $this->addPortfolio($package);

        return response(['error' => false, 'package' => $data]);
    }

    private function addMembers($package): array
    {
        $titleController = new TitleController();
        $artistController = new ArtistController();
        $artists = $package->artists;
        $data = [];
        foreach ($artists as $artist) {
            $artistInfo = $artistController->assignArtists([$artist])[0];
            $artistPackage = ArtistPackage::query()
                ->where('package_id', $package->id)
                ->where('artist_id', $artist->id)
                ->first();
            $titles = TitlePackage::query()
                ->where('artist_package_id', $artistPackage->id)
                ->get();
            $titleInfo = [];
            foreach ($titles as $title) {
                $title = Title::find($title->title_id);
                $titleInfo_ = $titleController->assignTitles([$title])[0];
                array_push($titleInfo, [
                    'id' => $titleInfo_['id'],
                    'name' => $titleInfo_['name'],
//                    'link' => $titleInfo_['link'],
                ]);
            }
            array_push($data, [
                'id' => $artistInfo['id'],
                'first_name' => $artistInfo['first_name'],
                'last_name' => $artistInfo['last_name'],
                'avatar' => $artistInfo['avatar'],
                'artist_link' => $artistInfo['link'],
                'titles' => $titleInfo
            ]);
        }
        return $data;
    }

    private function addPortfolio($package)
    {
        $portfolios = $package->contents;
        $this->putImageURLInObjects($portfolios, ['sound', 'image']);
        return $portfolios;
    }

    public function assignPackages($packages, $beApproved = true): array
    {
        $data = [];
        foreach ($packages as $package) {
            if ($beApproved) {
                if ($package->is_viewable != true)
                    continue;
            }
            array_push($data, [
                'name' => $package->name,
                'image' => $this->createURLForImage($package->image),
//                'link' => (new CreateLinkController)->__invoke('package', $package->id),
                'id' => $package->id
            ]);
        }
        return $data;
    }

//    public function isAllowedToBuy($id)
//    {
//        return Package::where('id', $id)
//            ->where('is_active', true)
//            ->where('is_viewable', true)
//            ->exists();
//    }

    public function findAllowedPackage($id)
    {
        return Package::where('id', $id)
            ->where('is_viewable', true)
//            ->where('is_active', true)
            ->first();
    }
}
