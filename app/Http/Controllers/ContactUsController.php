<?php

namespace App\Http\Controllers;

use App\Models\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class ContactUsController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|persian_alpha|max:150',
            'email' => 'required|email|max:150',
            'subject' => 'required|string|persian_alpha_eng_num|max:150',
            'description' => 'required|string|persian_alpha_eng_num|max:21500', //21500 char
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $trackingNo = $this->createTrackingNumber();

        ContactUs::query()->create([
            'full_name' => $request->input('full_name'),
            'email' => $request->input('email'),
            'subject' => $request->input('subject'),
            'description' => $request->input('description'),
            'tracking_no' => $trackingNo,
            'opened_at' => Carbon::now(),
        ]);

        $this->sendNotice('NEW_MSG) tracking no:' . $trackingNo);

        return response(['error' => false, 'messages' => ['پیام شما با شماره ی ' . $trackingNo . ' دریافت شد. از طریق ایمیل منتظر پاسخ کارشناسان ما باشید']]);
    }

    private function createTrackingNumber(): string
    {
        $t = $this->getNumber(8);
        while (ContactUs::query()->where('tracking_no', $t)->exists())
            $t = $this->getNumber(8);
        return $t;
    }
}
