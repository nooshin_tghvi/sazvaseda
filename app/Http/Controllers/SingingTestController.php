<?php

namespace App\Http\Controllers;


use App\Models\SingingTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Morilog\Jalali\Jalalian;

class SingingTestController extends Controller
{

    private string $pathPile;

    public function __construct()
    {
        $this->pathPile = 'singing_test/';
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'singing_file' => 'required|file|mimes:opus,ogg,3gp,wav,mp3|max:10000', //10000 KB
            'description' => 'nullable|string|max:21500', //21500 char,
        ]);
        if ($validator->fails()) {
            return response(['error' => true, 'messages' => $validator->errors()->all()]);
        }

        $singing = SingingTest::query()->create([
            'user_id' => $this->user()->id,
            'file' => $this->saveAndGetPathFile($this->pathPile, $request->file('singing_file')),
            'description' => $request->input('description'),
        ]);

        $this->sendNotice('NEW_SINGING_TEST) id:' . $singing->id);

        $singing->date = Jalalian::fromCarbon($singing->created_at)->format('%A, %d %B %Y');

        return response(['error' => false, 'singing' => $singing]);
    }

    public function mySingingTest()
    {
        $user = $this->user();
        $tests = $user->singingTests;
        foreach ($tests as $test)
            $test->date = Jalalian::fromCarbon($test->created_at)->format('%A, %d %B %Y');

        $tests = $this->putImageURLInObjects($tests, ['file']);
        return response(['error' => false, 'tests' => $tests]);
    }
}
