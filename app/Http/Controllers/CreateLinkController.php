<?php

namespace App\Http\Controllers;

class CreateLinkController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param $type
     * @param $id
     * @return string
     */
    public function __invoke($type, $id): string
    {
        return env('FRONT_URL') . '/api/' . $type . '/' . $id . '/detail';
    }
}
