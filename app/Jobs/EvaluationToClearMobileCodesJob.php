<?php

namespace App\Jobs;

use App\Models\SMSVerify;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


class EvaluationToClearMobileCodesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The podcast instance.
     *
     * @var SMSVerify
     */
    protected SMSVerify $smsVerify;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($smsVerify)
    {
        $this->smsVerify = $smsVerify;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $current = Carbon::now('Asia/Tehran');
        if ($current->gte($this->smsVerify->expiration_date))
            $this->smsVerify->delete();
    }
}
