<?php
return [
    "can_not_buy_package" => "تا اطلاع ثانوی نمیتوان پکیج :name را خریداری کرد",
    "can_not_reserve_advisor" => "تا اطلاع ثانوی هنرمند :name مشاوره نمی دهد",
    "can_not_reserve_studio" => "تا اطلاع ثانوی نمیتوان استدیو :name رزرو کرد",
    "change_cost_of_advisor" => "هزینه ی مشاوره با :name، :number تومان :sign شده است",
    "change_cost_of_package" => "قیمت خرید پکیج :name، :number تومان :sign شده است",
    "change_cost_of_studio" => "قیمت رزرو استدیو :name، :number تومان :sign شده است",
    "change_cost_of_teammate" => "هزینه ی همکاری با :name، :number تومان :sign شده است",
    "deactivate_artist" => "تا اطلاع ثانوی صفحه ی هنرمند :name غیر فعال است",
    "no_artist" => "ابتدا اطلاعات مربوط به ساخت صفحه ی هنرمند را تکمیل کنید",
    "not_accept_to_be_a_teammate" => "تا اطلاع ثانوی هنرمند :name درخواست همکاری نمی پذیرد",
    "select_wrong_studio" => "استدیو انتخابی تحت مالکیت شما نیست",
    "selected_wrong_image_for_studio" => "تصویر انتخابی برا استادیو شما نیست"
];
