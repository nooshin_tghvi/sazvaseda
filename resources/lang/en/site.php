<?php
return [
    "can_not_buy_package" => "The :name package cannot be purchased until further notice",
    "can_not_reserve_advisor" => "",
    "can_not_reserve_studio" => "",
    "change_cost_of_advisor" => "",
    "change_cost_of_package" => "",
    "change_cost_of_studio" => "",
    "change_cost_of_teammate" => "",
    "deactivate_artist" => "",
    "no_artist" => "First, fill in the information about creating an artist page",
    "not_accept_to_be_a_teammate" => "",
    "select_wrong_studio" => "The studio of your choice is not owned by you",
    "selected_wrong_image_for_studio" => "The selected image is not for your studio"
];
