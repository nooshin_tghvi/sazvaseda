<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_banks', function (Blueprint $table) {
            $table->id();
            $table->string('name', 75);
            $table->string('owner_type');
            $table->integer('owner_id');
            $table->enum('type', ['IMAGE', 'SOUND', 'URL']);
            $table->boolean('is_saleable')->default(false);
            $table->decimal('amount', 15, 0)->default(0);
            $table->text('description')->nullable();
            $table->date('date');
            $table->string('sound')->nullable();
            $table->string('image')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_banks');
    }
}
