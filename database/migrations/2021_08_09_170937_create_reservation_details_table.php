<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('res_date_id');  //Reservation Date id
            $table->foreign('res_date_id')->references('id')->on('reservation_dates')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->unsignedBigInteger('allowed_hr_id');        //Allowed Hours id
            $table->foreign('allowed_hr_id')->references('id')->on('allowed_hours')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->integer('score')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_details');
    }
}
