<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->unique();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->string('avatar', 191)->nullable();
            $table->enum('status', ['PENDING', 'APPROVAL', 'FAILED'])->default('APPROVAL'); //for Admin //todo change
            $table->text('experience')->nullable();
            $table->text('order_description')->nullable();
            $table->decimal('delivery_time', 3, 0)->default(0);
            $table->boolean('is_advisor')->default(false);
            $table->decimal('advise_price', 15, 0)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
