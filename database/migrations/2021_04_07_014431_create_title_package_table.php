<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTitlePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('title_package', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('artist_package_id');
            $table->foreign('artist_package_id')->references('id')->on('artist_package')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->unsignedBigInteger('title_id');
            $table->foreign('title_id')->references('id')->on('titles')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->unique(['artist_package_id', 'title_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('title_package');
    }
}
