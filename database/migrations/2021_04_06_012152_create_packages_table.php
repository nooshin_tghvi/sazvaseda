<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('name',120)->unique();
            $table->unsignedBigInteger('user_id'); //Manager
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->decimal('price', 15, 0);
            $table->integer('delivery_time');
            $table->string('image');
            $table->text('description')->nullable();
            $table->integer('po_number')->default(0); //purchase number
            $table->boolean('is_active')->default(false); //for Manager
            $table->boolean('is_viewable')->default(false); //for Admin
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
