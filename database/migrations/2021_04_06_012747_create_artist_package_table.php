<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtistPackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_package', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('artist_id');
            $table->foreign('artist_id')->references('id')->on('artists')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->unsignedBigInteger('package_id');
            $table->foreign('package_id')->references('id')->on('packages')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->boolean('accept')->default(true); //todo change
            $table->unique(['artist_id', 'package_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artist_package');
    }
}
