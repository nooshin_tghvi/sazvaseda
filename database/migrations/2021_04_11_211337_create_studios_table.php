<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studios', function (Blueprint $table) {
            $table->id();
            $table->string('name', 120)->unique();
            $table->unsignedBigInteger('user_id'); //Owner
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->string('logo')->nullable();
            $table->string('address');
            $table->decimal('price', 15, 0);
            $table->text('description')->nullable();
            $table->enum('status', ['PENDING', 'APPROVAL', 'FAILED'])->default('APPROVAL'); //for Admin //todo change
            $table->boolean('is_active')->default(false); //for Owner
            $table->unsignedSmallInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studios');
    }
}
