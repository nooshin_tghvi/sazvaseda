<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTitleArtistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('title_artist', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('artist_id');
            $table->foreign('artist_id')->references('id')->on('artists')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedBigInteger('title_id');
            $table->foreign('title_id')->references('id')->on('titles')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('accept_order')->default(false);
            $table->decimal('order_price', 15, 0)->default(0);
            $table->string('description')->nullable();
            $table->unique(['artist_id', 'title_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('title_artist');
    }
}
