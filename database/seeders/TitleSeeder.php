<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = [
            [
                'id' => 1,
                'name' => 'آهنگساز',
                'description' => null,
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 2,
                'name' => 'نوازنده',
                'description' => null,
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 3,
                'name' => 'تنظیم‌کننده',
                'description' => null,
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 4,
                'name' => 'شاعر',
                'description' => null,
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 5,
                'name' => 'خواننده',
                'description' => null,
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 6,
                'name' => 'صدابردار',
                'description' => null,
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 7,
                'name' => 'استدیو',
                'description' => null,
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 8,
                'name' => 'میکس و مسترکار',
                'description' => null,
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
        ];

        DB::table('titles')->insert($titles);
    }
}
