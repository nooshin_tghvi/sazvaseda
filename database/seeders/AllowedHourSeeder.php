<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AllowedHourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allowedHours = [
            [
                'id' => 1,
                'started_at' => '10:00:00',
                'ended_at' => '10:30:00',
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 2,
                'started_at' => '10:30:00',
                'ended_at' => '11:00:00',
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 3,
                'started_at' => '11:00:00',
                'ended_at' => '11:30:00',
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 4,
                'started_at' => '11:30:00',
                'ended_at' => '12:00:00',
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 5,
                'started_at' => '12:00:00',
                'ended_at' => '12:30:00',
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 6,
                'started_at' => '12:30:00',
                'ended_at' => '13:00:00',
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 7,
                'started_at' => '13:00:00',
                'ended_at' => '13:30:00',
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
            [
                'id' => 8,
                'started_at' => '13:30:00',
                'ended_at' => '14:00:00',
                'created_at' => '2021-07-22 13:38:36',
                'updated_at' => '2021-07-22 13:38:36'
            ],
        ];

        DB::table('allowed_hours')->insert($allowedHours);
    }
}
