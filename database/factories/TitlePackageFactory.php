<?php

namespace Database\Factories;

use App\Models\ArtistPackage;
use App\Models\Title;
use App\Models\TitlePackage;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

class TitlePackageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TitlePackage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        $artistPackage = ArtistPackage::all('id');
        $titles = Title::all('id');
        return [
            'artist_package_id' => $titles[random_int(1, sizeof($artistPackage) - 1)]->id,
            'title_id' => $titles[random_int(1, sizeof($titles) - 1)]->id,
        ];
    }
}
