<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\Studio;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Studio::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition(): array
    {
        $users = User::all('id');
        $cities = City::all('id');
        return [
            'name' => 'استدیو ' . $this->faker->unique()->firstName,
            'user_id' => $users[random_int(1, sizeof($users) - 1)]->id,
//            'logo' => 'public/faker/logo_studio.png',
            'address' => $this->faker->address,
            'price' => $this->faker->numberBetween(100, 700) * 100,
            'is_active' => $this->faker->boolean,
            'status' => $this->faker->randomElement(['PENDING', 'APPROVAL', 'FAILED']),
            'city_id' => $cities[$this->faker->numberBetween(1, sizeof($cities) - 1)]->id,
        ];
    }
}
