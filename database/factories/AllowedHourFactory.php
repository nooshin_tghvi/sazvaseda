<?php

namespace Database\Factories;

use App\Models\AllowedHour;
use Illuminate\Database\Eloquent\Factories\Factory;

class AllowedHourFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AllowedHour::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
