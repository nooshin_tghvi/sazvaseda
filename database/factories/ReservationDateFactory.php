<?php

namespace Database\Factories;

use App\Models\ReservationDate;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReservationDateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ReservationDate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
