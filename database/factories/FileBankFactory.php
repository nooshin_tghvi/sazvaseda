<?php

namespace Database\Factories;

use App\Models\Artist;
use App\Models\FileBank;
use App\Models\Package;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

class FileBankFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FileBank::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        $artists = Artist::all('id');
        $packages = Package::all('id');
        $bool = $this->faker->boolean;
        $type = $bool ? 'App\Models\Artist' : 'App\Models\Package';
        $id = $bool ? $artists[random_int(1, sizeof($artists) - 1)]->id : $packages[random_int(1, sizeof($packages) - 1)]->id;
        return [
            'name' => $this->faker->streetAddress,
            'owner_type' => $type,
            'owner_id' => $id,
            'type' => 'Image',
            'image' => 'faker/file_bank/' . $this->faker->randomNumber(1, 30) . '.jpg',
            'is_saleable' => false,
            'description' => 'توضیحات... ' . $this->faker->address,
            'date' => $this->faker->date(),
        ];
    }
}
