<?php

namespace Database\Factories;

use App\Models\Artist;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArtistFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Artist::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $boolean = $this->faker->boolean;
        return [
            'user_id' => $this->faker->unique()->numberBetween(1, 60),
            'avatar' => 'faker/person/' . $this->faker->randomNumber(1, 15) . '.jpg',
            'experience' => 'سوابق... ' . $this->faker->address,
            'order_description' => 'توضیحات پذیرش همکاری... ' . $this->faker->address,
            'delivery_time' => $this->faker->numberBetween(3, 10),
            'is_advisor' => $boolean,
            'advise_price' => $boolean ?
                $this->faker->numberBetween(2, 15) * 10000 : 0,
        ];
    }
}
