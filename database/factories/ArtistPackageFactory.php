<?php

namespace Database\Factories;

use App\Models\Artist;
use App\Models\ArtistPackage;
use App\Models\Package;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArtistPackageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ArtistPackage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition(): array
    {
        $packages = Package::all('id');
        $artists = Artist::all('id');
        return [
            'artist_id' => $artists[random_int(1, sizeof($artists) - 1)]->id,
            'package_id' => $packages[random_int(1, sizeof($packages) - 1)]->id,
            'accept' => $this->faker->boolean,
        ];
    }
}
