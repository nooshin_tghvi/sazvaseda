<?php

namespace Database\Factories;

use App\Models\Artist;
use App\Models\Title;
use App\Models\TitleArtist;
use Illuminate\Database\Eloquent\Factories\Factory;

class TitleArtistFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TitleArtist::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition(): array
    {
        $artists = Artist::all('id');
        $titles = Title::all('id');
        $boolean = $this->faker->boolean;
        return [
            'artist_id' => $artists[random_int(1, sizeof($artists) - 1)]->id,
            'title_id' => $titles[random_int(1, sizeof($titles) - 1)]->id,
            'accept_order' => $boolean,
            'order_price' => $boolean ?
                $this->faker->numberBetween(1, 5) * 1000000 : 0,
        ];
    }
}
