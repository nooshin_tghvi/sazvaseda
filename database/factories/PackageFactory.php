<?php

namespace Database\Factories;

use App\Models\Package;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PackageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Package::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition(): array
    {
        $users = User::all('id');
        return [
            'name' => 'پکیج '.$this->faker->unique()->firstName,
            'user_id' => $users[random_int(1, sizeof($users) - 1)]->id,
            'price' => $this->faker->numberBetween(100, 500) * 100,
            'delivery_time' => $this->faker->numberBetween(1, 5) * 10,
            'image' => 'faker/package/' . $this->faker->randomNumber(1, 10) . '.jpg',
            'is_active' => $this->faker->boolean,
            'is_viewable' => $this->faker->boolean,
        ];
    }
}
