<?php

namespace Database\Factories;

use App\Models\Image;
use App\Models\Studio;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Image::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        $studios = Studio::all('id');
        $id = $studios[random_int(1, sizeof($studios) - 1)]->id;
        return [
            'name' => $this->faker->streetAddress,
            'description' => 'توضیحات... ' . $this->faker->address,
            'owner_type' => 'App\Models\Studio',
            'owner_id' => $id,
            'path' => 'faker/image/' . $this->faker->randomNumber(1, 23) . '.jpg',
        ];
    }
}
